<!DOCTYPE html>


<html lang="pt-br" xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="{{ $dados['palavrasChave'] }}">
	<meta name="description" content="{{ $dados['descricaoGoogle'] }}">
	
	<title>{{$dados['nome']}}</title>

	<!-- ESTILOS -->
	<link href="{{ asset('css/estilo.css') }}" rel="stylesheet">
	<link href="{{ asset('css/imobiliaria.css') }}" rel="stylesheet">
	<link href="{{ asset('css/menu.css') }}" rel="stylesheet">
	<link href="{{ asset('css/pgwslider.css')}}" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	
	<!-- ICONES -->
	<script src="https://use.fontawesome.com/34dec068ab.js"></script>

	<!-- BXSlider -->
	<link href="{{ asset('css/jquery.bxslider.css')}}" rel="stylesheet" />

	<!-- FAVICON -->
	<link rel="shortcut icon" type="image/png" href="{{asset('img/favicon.png')}}">

	<!-- FONTES -->
	<link href="https://fonts.googleapis.com/css?family=Hind+Madurai" rel="stylesheet"> 
	<link href="https://fonts.googleapis.com/css?family=Francois+One" rel="stylesheet"> 
	<link href="http://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,300,700" rel="stylesheet" type="text/css">
	
	<!-- LIGHTBOX -->
	<link href="{{ asset('css/lightbox.css') }}" rel="stylesheet">


</head>
<body>
	<header>
		<div class="container">
			<div class="row">
				<div class="col-md-5 ">
					<div class="logo">
						<a href="{{ route('main') }}">
							<img src="{{asset('img/logo.png')}}" class="img-responsive" alt="">
						</a>
					</div>
				</div>

				
				<div class="dados-topo hidden-sm hidden-xs">
					<div class="col-md-offset-1 col-md-3 hidden-sm hidden-xs">
						<div class="box_telefone">
							<div class="t-telefone"><i class="fa fa-phone-square"></i> Telefone</div>
							<div class="n-telefone">{{ $dados['telefone']}}</div>
						</div>
					</div>

					<div class="col-md-3 hidden-sm hidden-xs">
						<div class="box_whatsapp">
							<div class="t-whatsapp"><i class="fa fa-whatsapp"></i> WhatsApp</div>
							<div class="n-whatsapp">{{$dados['whatsapp']}}</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		

		<nav id="menu-principal" class="navbar" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse" data-target=".navbar-cat-collapse">
						<span class="sr-only">Toggle Navigation</span>
						<i class="fa fa-bars"></i>
					</button>
				</div>
				<div class="collapse navbar-collapse navbar-cat-collapse">
					<ul class="nav navbar-nav">
						<li>{!! link_to_route('main', 'Início') !!}</li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="10">
								Imóveis
							</a>
							<ul class="dropdown-menu" role="menu">
								@foreach($tipos as $tipo)
									<li>{!! link_to_route('buscarPorTipo', $tipo->nome, [$tipo->id]) !!}</li>
								@endforeach
							</ul>
						</li>

						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="10">
								Finalidade
							</a>
							<ul class="dropdown-menu" role="menu">
								<li>{!! link_to_route('buscarPorFinalidade', "Quero comprar", 1) !!}</li>
								<li>{!! link_to_route('buscarPorFinalidade', "Quero alugar", 2) !!}</li>
							</ul>
						</li>

						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="10">
								Simule seu financiamento
							</a>
							<ul class="dropdown-menu" role="menu">
								@foreach($simulacoes as $simulacao)
									<li>{!! link_to_route('buscarSimulacao', $simulacao->nome, [$simulacao->id]) !!}</li>
								@endforeach
							</ul>
						</li>

						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="10">
								Serviço
							</a>
							<ul class="dropdown-menu" role="menu">
								@foreach($servicos as $servico)
								<li>
									{!! link_to_route('buscarServico', $servico->nome, $servico->id) !!}
								</li>
								@endforeach
							</ul>
						</li>

						<li>{!! link_to_route('depoimentos', 'Depoimentos') !!}</li>
						<li>{!! link_to_route('onde-estamos', 'Onde Estamos') !!}</li>
						<li>{!! link_to_route('empresa', 'Sobre nós') !!}</li>
						<li>{!! link_to_route('contato', 'Contato') !!}</li>
					</ul>
				</div>
			</div>
		</nav>
	</header>

	<a href="" data-toggle="modal" data-target="#ligacao" class="canto hidden-xs hidden-sm"></a>

	@if(Session::has('success'))
	    <div class="alert alert-info">
	        {{ Session::get('success') }}
	    </div>
	@endif

	@if ($errors->any())
	    <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif

    @yield('corpo')
    
@include('inc.modal.ligacao')

<div class="linha"></div>
<section class="infos">
	<div class="container">
		<div class="caixa_infos">
			<div class="row">
				<div class="col-md-6 bot20">
					<div class="t-facebook"><i class="fa fa-share-alt"></i> Facebook</div>
					<div class="box_facebook">
						<div id="fb-root"></div>
						<script>(function(d, s, id) {
							var js, fjs = d.getElementsByTagName(s)[0];
							if (d.getElementById(id)) return;
							js = d.createElement(s); js.id = id;
							js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.10&appId=266826787008999";
							fjs.parentNode.insertBefore(js, fjs);
						}(document, 'script', 'facebook-jssdk'));</script>

						<div class="fb-page" data-href="https://www.facebook.com/kodevsolucoesempresariais/" data-width="500" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/kodevsolucoesempresariais/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/kodevsolucoesempresariais/">Kodev - Soluções Empresariais</a></blockquote></div>
					</div>
				</div>

				<div class="col-md-6">
					<div class="t-facebook"><i class="fa fa-map-marker"></i> Onde Estamos</div>
					<div class="box_mapa">
						<a href="{{ route('onde-estamos') }}">
							<img src="{{asset('img/localizacao.jpg')}}" class="thumbnail img-responsive" alt="">
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="linha"></div>

<section class="listas">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<h1 class="titulo">Imóveis</h1>
				<div class="lista">
					@foreach($tipos as $tipo)
						<li>
							{!! link_to_route('buscarPorTipo', $tipo->nome, $tipo->id) !!}
						</li>
					@endforeach
				</div>
			</div>

			<div class="col-md-3">
				<h1 class="titulo">Finalidade</h1>
				<div class="lista">
					<li>{!! link_to_route('buscarPorFinalidade', "Quero comprar", 1) !!}</li>
					<li>{!! link_to_route('buscarPorFinalidade', "Quero alugar", 2) !!}</li>
				</div>
			</div>

			<div class="col-md-3">
				<h1 class="titulo">Simulação</h1>
				<div class="lista">
					@foreach($simulacoes as $simulacao)
						<li>{!! link_to_route('buscarSimulacao', $simulacao->nome, [$simulacao->id]) !!}</li>
					@endforeach
				</div>
			</div>

			<div class="col-md-3">
				<h1 class="titulo">Serviço</h1>
				<div class="lista">
					@foreach($servicos as $servico)
					<li>
						{!! link_to_route('buscarServico', $servico->nome, $servico->id) !!}
					</li>
					@endforeach
				</div>
			</div>
		</div>
	</div>
</section>

<footer class="rodape">
	<div class="container">

		<div class="contato-footer">
			<div class="row">
				<div class="col-md-4">
					<div class="telefone-footer">
						<div class="i-footer"><i class="fa fa-phone-square"></i></div>
						<div class="t-footer">Telefone</div>
						<div class="c-footer">{{ $dados['telefone'] }}</div>
					</div>
				</div>

				<div class="col-md-4">
					<div class="telefone-footer">
						<div class="i-footer"><i class="fa fa-whatsapp"></i></div>
						<div class="t-footer">WhatsApp</div>
						<div class="c-footer">{{ $dados['whatsapp'] }}</div>
					</div>
				</div>

				<div class="col-md-4">
					<div class="telefone-footer">
						<div class="i-footer"><i class="fa fa-envelope"></i></div>
						<div class="t-footer">E-mail</div>
						<div class="c-footer">{{ $dados['email'] }}</div>
					</div>
				</div>
			</div>
		</div>

		<div class="copy-footer">
			<div class="row">
				<div class="col-md-12 centro">
					<div class="copy">
						&copy; <?php echo date('Y'); ?>  - Todos os direitos reservados.
					</div>

					<div class="kodev">
						<a href="http://kodev.com.br" target="_blank">
							<img src="{{asset('img/kodev.png')}}" alt="Kodev">
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="{{asset('js/jquery.bxslider.min.js')}}"></script>
<script src="{{asset('js/slide.js')}}"></script>
<script src="{{asset('js/bootstrap.js')}}"></script>
<script src="{{asset('js/menu.js')}}"></script>
<script src="{{asset('js/piscar.js')}}"></script>
<script src="{{asset('js/tema.js')}}"></script>
<script src="{{asset('js/lightbox.js')}}"></script>
<script src="{{asset('js/pgwslider.js')}}"></script>


</body>
</html>
