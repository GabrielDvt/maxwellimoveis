@extends('comum')

@section('corpo')

<section class="pagina">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1 class="titulo">{{ $titulo }}</h1>
				</div>
			</div>
	
			<div class="row">
				<!-- INICIO -->
				@foreach($imoveis as $imovel)
				<div class="col-md-4">
						<div class="imovel-exibicao thumbnail">
							<div class="row">
								<div class="col-sm-12">
									<div class="faixa">
										<img src="{{ asset('public/storage/' . $imovel->urlImagem) }}" alt="" />
										<span class="faixa-padrao">
											
											@if($imovel->vendido == 1)
												Vendido
											@else
												@if($imovel->finalidade == 1)
													Venda
												@else
													Aluguel
												@endif
											@endif
											
										</span>
									</div>
		
									<div class="espacamento-imovel">
										<div class="titulo-imovel-ex">
											{!! link_to_route('buscarImovel', $imovel->nome, $imovel->id) !!}
										</div>
		
										<div class="desc-imovel-ex">
											{!! substr($imovel->descricao, 0, 255) . "..." !!}
										</div>
		
										<div class="preco-imovel-ex">
											R$ {{ $imovel->valor }}
										</div>
		
										<div class="localizacao-imovel-ex">
											{{ $imovel->cidade->nome }}
										</div>
		
										<div class="mais-info">
											{!! link_to_route('buscarImovel', 'Mais Informações', $imovel->id, ['class' => 'btn botao']) !!}
										</div>
		
									</div>
		
									<div class="info-imovel-ex">
										<div class="row">
											<div class="icones-info">
												<div class="col-sm-3">
													<div class="i-metros" data-toggle="tooltip" data-placement="top" title="Metros Quadrados"> {{ $imovel->area }}m²</div>
												</div>
		
												<div class="col-sm-3">
													<div class="i-quartos" data-toggle="tooltip" data-placement="top" title="Quartos"> {{ $imovel->quartos }}</div>
												</div>
		
												<div class="col-sm-3">
													<div class="i-banheiros" data-toggle="tooltip" data-placement="top" title="Banheiros"> {{ $imovel->banheiros }}</div>
												</div>
		
												<div class="col-sm-3">
													<div class="i-garagens" data-toggle="tooltip" data-placement="top" title="Garagens">{{ $imovel->garagens }}</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				@endforeach
				
				<!-- FIM -->
	
			</div>
		</div>
	</section>
@endsection
