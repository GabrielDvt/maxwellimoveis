<?php $pagina = "Empresa"; ?>
<?php $menu = "empresa"; ?>
@extends('comum')

@section('corpo')
	<section class="pagina">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1 class="titulo"><?php echo $pagina ?></h1>
					{!! $obj->texto !!}
				</div>
			</div>
		</div>
	</section>
@endsection