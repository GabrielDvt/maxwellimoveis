@extends('comum')

@section('corpo')
	<section class="pagina">
		<div class="container">
			<div class="row">
				<div class="col-md-9">
					<h1 class="titulo">{{ $simulacao->nome }}</h1>

					<div class="row">
						<div class="col-sm-8">
							<div class="descricao_servico">
								<a target="_blank" href="{{ $simulacao->link }}">Link para simulação</a>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</section>
@endsection