<?php $pagina = "Contato"; ?>
<?php $menu = "contato"; ?>
@extends('comum')

@section('corpo')



<section class="pagina">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="titulo">{{ $pagina }}</h1>

				<div class="form-contato">
					{!! Form::open(array('route' => 'sendContato', 'method' => 'POST')) !!}
						{{ csrf_field() }}
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<div class="t-form">Seu Nome</div>
									<input type="text" name="nome" required="" id="" class="form-control2">
								</div>
							</div>

							<div class="col-sm-6">
								<div class="form-group">
									<div class="t-form">Seu E-mail</div>
									<input type="email" name="email" required="" id="" class="form-control2">
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<div class="t-form">Assunto</div>
									<input type="text" name="assunto" required="" id="" class="form-control2">
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<div class="t-form">Mensagem</div>
									<textarea name="" required="mensagem" id="" cols="30" rows="10" style="height: 300px;" class="form-control2"></textarea>
								</div>
							</div>

							<div class="col-sm-12">
								<div class="form-group">
									<input type="submit" class="btn botao" value="Enviar mensagem">
								</div>
							</div>
						</div>
					{!! Form::close() !!}
				</div>

			</div>
		</div>
	</div>
</section>
@endsection