<?php $pagina = "Nossos serviços"; ?>
<?php $menu = "servicos"; ?>
@extends('comum')


@section('corpo')

	<section class="pagina">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1 class="titulo">{{ $pagina }}</h1>
				</div>
			</div>

			<div class="row">
				
				<!-- INICIO -->
				<div class="col-md-4">
					<div class="box_servico thumbnail">
						<div class="servico">
							<a href="">
								<img src="img/servico/ex.jpg" class="img-responsive" alt="">
							</a>
						</div>

						<div class="info_servico">
							<div class="nome_servico">Nome do serviço</div>
							<div class="breve_descricao">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo iusto impedit...</div>
							<a href="servico" type="button" class="btn btn-sm botao">Mais Informações</a>
						</div>
					</div>
				</div>
				<!-- FIM -->

				<!-- INICIO -->
				<div class="col-md-4">
					<div class="box_servico thumbnail">
						<div class="servico">
							<a href="">
								<img src="img/servico/ex.jpg" class="img-responsive" alt="">
							</a>
						</div>

						<div class="info_servico">
							<div class="nome_servico">Nome do serviço</div>
							<div class="breve_descricao">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo iusto impedit...</div>
							<a href="servico" type="button" class="btn btn-sm botao">Mais Informações</a>
						</div>
					</div>
				</div>
				<!-- FIM -->

				<!-- INICIO -->
				<div class="col-md-4">
					<div class="box_servico thumbnail">
						<div class="servico">
							<a href="">
								<img src="img/servico/ex.jpg" class="img-responsive" alt="">
							</a>
						</div>

						<div class="info_servico">
							<div class="nome_servico">Nome do serviço</div>
							<div class="breve_descricao">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo iusto impedit...</div>
							<a href="servico" type="button" class="btn btn-sm botao">Mais Informações</a>
						</div>
					</div>
				</div>
				<!-- FIM -->

				<!-- INICIO -->
				<div class="col-md-4">
					<div class="box_servico thumbnail">
						<div class="servico">
							<a href="">
								<img src="img/servico/ex.jpg" class="img-responsive" alt="">
							</a>
						</div>

						<div class="info_servico">
							<div class="nome_servico">Nome do serviço</div>
							<div class="breve_descricao">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo iusto impedit...</div>
							<a href="servico" type="button" class="btn btn-sm botao">Mais Informações</a>
						</div>
					</div>
				</div>
				<!-- FIM -->

				<!-- INICIO -->
				<div class="col-md-4">
					<div class="box_servico thumbnail">
						<div class="servico">
							<a href="">
								<img src="img/servico/ex.jpg" class="img-responsive" alt="">
							</a>
						</div>

						<div class="info_servico">
							<div class="nome_servico">Nome do serviço</div>
							<div class="breve_descricao">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo iusto impedit...</div>
							<a href="servico" type="button" class="btn btn-sm botao">Mais Informações</a>
						</div>
					</div>
				</div>
				<!-- FIM -->

				<!-- INICIO -->
				<div class="col-md-4">
					<div class="box_servico thumbnail">
						<div class="servico">
							<a href="">
								<img src="img/servico/ex.jpg" class="img-responsive" alt="">
							</a>
						</div>

						<div class="info_servico">
							<div class="nome_servico">Nome do serviço</div>
							<div class="breve_descricao">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo iusto impedit...</div>
							<a href="servico" type="button" class="btn btn-sm botao">Mais Informações</a>
						</div>
					</div>
				</div>
				<!-- FIM -->

			</div>
		</div>
	</section>

@endsection
