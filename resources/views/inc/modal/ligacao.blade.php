<div class="modal fade" id="ligacao" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Nós ligamos para você!</h4>
			</div>
			<div class="modal-body">

				<div class="row">
					<div class="col-md-12">
					@if(Session::has('success'))
						<div class="alert alert-success centro" role="alert"><i class="fa fa-check"></i></div>
					@endif
					</div>
				</div>
				
				<div class="row">
					<div class="col-lg-8"><div class="alert alert-info" role="alert">Preencha os campos abaixo para que nós possamos ligar para você!</div>
					{!! Form::open(array('route' => 'enviarSolicitacao', 'method' => 'POST')) !!}
						{{ csrf_field() }}
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<div class="t-form">Seu Nome</div>
									<input type="text" name="nome" id="" class="form-control2" required="">
								</div>
							</div>

							<div class="col-sm-6">
								<div class="form-group">
									<div class="t-form">Seu Email</div>
									<input type="email" name="email" id="" class="form-control2" required="">
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<div class="row">
										<div class="col-sm-12">
											<div class="t-form">Seu telefone</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-3">
											<input type="tel" placeholder="DDD" name="ddd" id="" class="form-control2" required="">
										</div>

										<div class="col-sm-9">
											<input type="tel" placeholder="Telefone" name="telefone" id="" class="form-control2" required="">
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<div class="t-form">Qual o melhor horário?</div>
									<select name="horario" id="" class="form-control2" required="">
										<option value="09:00h - 10:00h">09:00h - 10:00h</option>
										<option value="11:00h - 12:00h">11:00h - 12:00h</option>
										<option value="13:00h - 14:00h">13:00h - 14:00h</option>
										<option value="15:00h - 16:00h">15:00h - 16:00h</option>
										<option value="17:00h - 18:00h">17:00h - 18:00h</option>
									</select>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12 centro">
								<div class="form-group">
									<input type="submit" name="" value="Enviar solicitação" class="btn btn-lg botao">
								</div>
							</div>
						</div>

					{!! Form::close() !!}

					</div>
					<div class="col-lg-4">
						<img src="img/mulher-ligacao.png" class="img-responsive" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>