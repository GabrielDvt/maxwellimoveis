@include('inc.modal.ligacao')

<?php
$nome = "";
$telefone = "(00) 0000-0000"; 
$whatsapp = "(00) 0000-0000"; 
$email = "";
?>

<div class="linha"></div>
<section class="infos">
	<div class="container">
		<div class="caixa_infos">
			<div class="row">
				<div class="col-md-6 bot20">
					<div class="t-facebook"><i class="fa fa-share-alt"></i> Facebook</div>
					<div class="box_facebook">
						<div id="fb-root"></div>
						<script>(function(d, s, id) {
							var js, fjs = d.getElementsByTagName(s)[0];
							if (d.getElementById(id)) return;
							js = d.createElement(s); js.id = id;
							js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.10&appId=266826787008999";
							fjs.parentNode.insertBefore(js, fjs);
						}(document, 'script', 'facebook-jssdk'));</script>

						<div class="fb-page" data-href="https://www.facebook.com/kodevsolucoesempresariais/" data-width="500" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/kodevsolucoesempresariais/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/kodevsolucoesempresariais/">Kodev - Soluções Empresariais</a></blockquote></div>
					</div>
				</div>

				<div class="col-md-6">
					<div class="t-facebook"><i class="fa fa-map-marker"></i> Onde Estamos</div>
					<div class="box_mapa">
						<a href="onde-estamos">
							<img src="img/localizacao.jpg" class="thumbnail img-responsive" alt="">
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="linha"></div>

<section class="listas">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<h1 class="titulo">Imóveis</h1>
				<div class="lista">
					<li><a href="">Exemplo</a></li>
					<li><a href="">Exemplo</a></li>
					<li><a href="">Exemplo</a></li>
					<li><a href="">Exemplo</a></li>
					<li><a href="">Exemplo</a></li>
					<li><a href="">Exemplo</a></li>
				</div>
			</div>

			<div class="col-md-3">
				<h1 class="titulo">Finalidade</h1>
				<div class="lista">
					<li><a href="">Quero alugar</a></li>
					<li><a href="">Quero comprar</a></li>
				</div>
			</div>

			<div class="col-md-3">
				<h1 class="titulo">Simulação</h1>
				<div class="lista">
					<li><a href="">Banco do Brasil</a></li>
					<li><a href="">Caixa Econômica Federal</a></li>
				</div>
			</div>

			<div class="col-md-3">
				<h1 class="titulo">Serviço</h1>
				<div class="lista">
					<li><a href="">Exemplo</a></li>
					<li><a href="">Exemplo</a></li>
					<li><a href="">Exemplo</a></li>
					<li><a href="">Exemplo</a></li>
				</div>
			</div>
		</div>
	</div>
</section>

<footer class="rodape">
	<div class="container">

		<div class="contato-footer">
			<div class="row">
				<div class="col-md-4">
					<div class="telefone-footer">
						<div class="i-footer"><i class="fa fa-phone-square"></i></div>
						<div class="t-footer">Telefone</div>
						<div class="c-footer"><?php echo $telefone ?></div>
					</div>
				</div>

				<div class="col-md-4">
					<div class="telefone-footer">
						<div class="i-footer"><i class="fa fa-whatsapp"></i></div>
						<div class="t-footer">WhatsApp</div>
						<div class="c-footer"><?php echo $whatsapp ?></div>
					</div>
				</div>

				<div class="col-md-4">
					<div class="telefone-footer">
						<div class="i-footer"><i class="fa fa-envelope"></i></div>
						<div class="t-footer">E-mail</div>
						<div class="c-footer"><?php echo $email ?></div>
					</div>
				</div>
			</div>
		</div>

		<div class="copy-footer">
			<div class="row">
				<div class="col-md-12 centro">
					<div class="copy">
						&copy; <?php echo date('Y'); ?> <?php echo $nome ?> - Todos os direitos reservados.
					</div>

					<div class="kodev">
						<a href="http://kodev.com.br" target="_blank">
							<img src="img/kodev.png" alt="Kodev">
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>



<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="js/jquery.bxslider.min.js"></script>
<script src="js/slide.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/menu.js"></script>
<script src="js/piscar.js"></script>
<script src="js/tema.js"></script>
<script src="js/lightbox.js"></script>
<script src="js/pgwslider.js"></script>


</body>
</html>