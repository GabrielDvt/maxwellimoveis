<?php

$nome = "";
$telefone = "(00) 0000-0000"; 
$whatsapp = "(00) 0000-0000"; 
$email = "";

?>

<!DOCTYPE html>
<html lang="pt-br" xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="">
	<meta name="description" content="">

	<title><?php echo $dados->nome ?> - <?php echo $pagina ?></title>

	<!-- ESTILOS -->
	<link href="css/estilo.css" rel="stylesheet">
	<link href="css/imobiliaria.css" rel="stylesheet">
	<link href="css/menu.css" rel="stylesheet">
	<link href="css/pgwslider.css" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	
	<!-- ICONES -->
	<script src="https://use.fontawesome.com/34dec068ab.js"></script>

	<!-- BXSlider -->
	<link href="css/jquery.bxslider.css" rel="stylesheet" />

	<!-- FAVICON -->
	<link rel="shortcut icon" type="image/png" href="img/favicon.png">

	<!-- FONTES -->
	<link href="https://fonts.googleapis.com/css?family=Hind+Madurai" rel="stylesheet"> 
	<link href="https://fonts.googleapis.com/css?family=Francois+One" rel="stylesheet"> 
	<link href="http://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,300,700" rel="stylesheet" type="text/css">
	
	<!-- LIGHTBOX -->
	<link href="css/lightbox.css" rel="stylesheet">


</head>
<body>
	<header>
		<div class="container">
			<div class="row">
				<div class="col-md-5 ">
					<div class="logo">
						<a href="./">
							<img src="img/logo.png" class="img-responsive" alt="">
						</a>
					</div>
				</div>

				<div class="dados-topo hidden-sm hidden-xs">
					<div class="col-md-offset-1 col-md-3 hidden-sm hidden-xs">
						<div class="box_telefone">
							<div class="t-telefone"><i class="fa fa-phone-square"></i> Telefone</div>
							<div class="n-telefone">{{ $dados->telefone }}</div>
						</div>
					</div>

					<div class="col-md-3 hidden-sm hidden-xs">
						<div class="box_whatsapp">
							<div class="t-whatsapp"><i class="fa fa-whatsapp"></i> WhatsApp</div>
							<div class="n-whatsapp">{{ $dados->whatsapp }}</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<nav id="menu-principal" class="navbar" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse" data-target=".navbar-cat-collapse">
						<span class="sr-only">Toggle Navigation</span>
						<i class="fa fa-bars"></i>
					</button>
				</div>
				<div class="collapse navbar-collapse navbar-cat-collapse">
					<ul class="nav navbar-nav">
						<li><a href="./">Início</a></li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="10">
								Imóveis
							</a>
							<ul class="dropdown-menu" role="menu">
								<li><a tabindex="-1" href="tipo">Tipo A</a></li>
								<li><a tabindex="-1" href="tipo">Tipo B</a></li>
								<li><a tabindex="-1" href="tipo">Tipo C</a></li>
							</ul>
						</li>

						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="10">
								Finalidade
							</a>
							<ul class="dropdown-menu" role="menu">
								<li><a tabindex="-1" href="categoria">Quero comprar</a></li>
								<li><a tabindex="-1" href="categoria">Quero alugar</a></li>
							</ul>
						</li>

						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="10">
								Simule seu financiamento
							</a>
							<ul class="dropdown-menu" role="menu">
								<li><a tabindex="-1" href="#">Banco Exemplo</a></li>
								<li><a tabindex="-1" href="#">Banco Exemplo</a></li>
								<li><a tabindex="-1" href="#">Banco Exemplo</a></li>
								<li><a tabindex="-1" href="#">Banco Exemplo</a></li>
							</ul>
						</li>

						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="10">
								Serviço
							</a>
							<ul class="dropdown-menu" role="menu">
								<li><a tabindex="-1" href="servico">Exemplo</a></li>
								<li><a tabindex="-1" href="servico">Exemplo</a></li>
								<li><a tabindex="-1" href="servico">Exemplo</a></li>
							</ul>
						</li>

						<li><a href="depoimentos">Depoimentos</a></li>
						<li><a href="onde-estamos">Onde Estamos</a></li>
						<li><a href="empresa">Sobre nós</a></li>
						<li><a href="contato">Contato</a></li>
					</ul>
				</div>
			</div>
		</nav>
	</header>

	<a href="" data-toggle="modal" data-target="#ligacao" class="canto hidden-xs hidden-sm"></a>

