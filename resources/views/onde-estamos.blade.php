<?php $pagina = "Onde Estamos"; ?>
<?php $menu = "localizacao"; ?>

@extends('comum')

@section('corpo')
<section class="pagina">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="titulo">{{ $pagina }}</h1>
				@foreach($unidades as $unidade)
					<div class="box_localizacao well">
						<div class="row">
							<div class="col-sm-4">
								<div class="foto_fachada">
									<a href="">
										<img src="img/localizacao/ex.jpg" class="img-localizacao" alt="">
									</a>
								</div>
							</div>

							<div class="col-sm-8">
								<h1 class="titulo2">Informações</h1>
								<div class="info_localizacao">
									<li><b>Unidade: </b> {{$unidade->unidade}}</li>
									<li><b>Telefone: </b> {{$unidade->telefone}}</li>
									<li><b>WhatApp: </b> {{$unidade->whatsapp}}</li>
									<li><b>Email: </b> {{$unidade->email}}</li>
									<li><b>Endereço: </b> {{$unidade->endereco}}</li>
									<li><b>Mapa on-line: </b> <a href="{{$unidade->linkMapa}}" target="_blank">Clique aqui para abrir o mapa on-line</a></li>
								</div>
							</div>
						</div>
					</div>
				@endforeach
			</div>
		</div>
	</div>
</section>

@endsection

