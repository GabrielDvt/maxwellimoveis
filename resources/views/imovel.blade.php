
<?php $menu = "empresa"; ?>
@extends('comum')

@section('corpo')
<section class="pagina">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1 class="titulo">{{ $imovel->nome }}</h1>
				</div>
	
				<div class="col-md-12">
					<ul class="pgwSlider">

						@foreach($imovel->imagens as $imagem)
							<li><img src="{{ asset('public/storage/' . $imagem->urlImagem) }}" class="img-responsive"></li>
						@endforeach

					</ul>
				</div>
			</div>
	
			<div class="row top20">
				<div class="col-md-12">
					<h1 class="titulo2">Descrição</h1>
					<p>{!! $imovel->descricao !!}</p>
				</div>
			</div>
	
			<div class="row fonte">
				<div class="col-md-4">
					<h1 class="titulo2">Informações</h1>
					<div class="info-icones">
						<li><b>Finalidade:</b>@if($imovel->finalidade == 1) Venda @else Aluguel @endif</li>
						<li><b>Preço:</b> R$ {{ $imovel->valor }}</li>
						<li><b>Cidade:</b> {{ $imovel->cidade->nome }}</li>
						<li><b>Bairro:</b> {{ $imovel->bairro }}</li>
						<li><b>Código do Imóvel:</b> <strong class="box label-success label-sm">{{ $imovel->referencia }}</strong></li>
					</div>
				</div>
	
				<div class="col-md-4">
					<h1 class="titulo2">Características</h1>
					<div class="caracteristicas-icones">
						<li class="metros">Metros quadrados: {{ $imovel->area }} m²</li>
						<li class="quartos">Quartos: {{ $imovel->quartos }}</li>
						<li class="banheiros">Banheiros: {{ $imovel->banheiros }}</li>
						<li class="banheiros">Suítes: {{ $imovel->suites }}</li>
						<li class="garagens">Garagem: {{ $imovel->garagens }}</li>
					</div>
				</div>
	
				<div class="col-md-4">
					<h1 class="titulo2">Adicionais</h1>
					<div class="adicionais">
						@foreach($imovel->atributos as $atributo)
							<li>{{ $atributo->nome }}</li>
						@endforeach
					</div>
				</div>
			</div>
	
			<div class="row">
				<div class="col-md-12">
					<h1 class="titulo">Tire as dúvidas sobre este imóvel direto com o corretor</h1>
					{!! Form::open(array('route' => 'sendEmailCorretor', 'method' => 'POST')) !!}
						{{ csrf_field() }}
						<input type="hidden" name="idImovel" value="{{ $imovel->id }}">
						<input type="hidden" name="nomeImovel" value="{{ $imovel->nome }}">
						<div class="row">
							<div class="col-sm-4">
								<div class="t-form">Seu nome</div>
								<input type="text" name="nome" id="" class="formulario" required="">
							</div>
	
							<div class="col-sm-4">
								<div class="t-form">E-mail</div>
								<input type="email" name="email" id="" class="formulario" required="">
							</div>
	
							<div class="col-sm-4">
								<div class="t-form">Telefone</div>
								<div class="row">
									<div class="col-sm-3">
										<input type="tel" placeholder="DDD" name="ddd" id="" class="formulario" required="">
									</div>
	
									<div class="col-sm-9">
										<input type="tel" placeholder="Telefone" name="telefone" id="" class="formulario" required="">
									</div>
								</div>
							</div>
						</div>
	
						<div class="row top20">
							<div class="col-sm-12">
								<div class="form-group">
									<div class="t-form">Qual a sua dúvida?</div>
									<textarea name="duvida" id="" style="height: 300px;" cols="30" rows="10" class="formulario"></textarea>
								</div>
							</div>
						</div>
	
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<button type="submit" class="btn botao">Enviar</button>
								</div>
							</div>
						</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</section>
	
@endsection