@extends('comum')

@section('corpo')
	<section class="pagina">
		<div class="container">
			<div class="row">
				<div class="col-md-9">
					<h1 class="titulo">{{ $servico->nome }}</h1>

					<div class="row">
						<div class="col-sm-4">
							<img src="{{asset('public/storage/' . $servico->urlImagem)}}" class="thumbnail img-responsive" alt="">
						</div>
						<div class="col-sm-8">
							<div class="descricao_servico">
								{!! $servico->descricao !!}
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-3">
					<h1 class="titulo">Mais serviços</h1>

				<ul class="list-group">
					@foreach($servicos as $servico)
						<li class="list-group-item">
							{!! link_to_route('buscarServico', $servico->nome, $servico->id, ['class' => 'categoria_menu']) !!}
						</li>
					@endforeach
				</ul>
				</div>
			</div>
		</div>
	</section>
@endsection