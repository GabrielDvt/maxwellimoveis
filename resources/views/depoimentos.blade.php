<?php $pagina = "Depoimentos"; ?>
<?php $menu = "depoimentos"; ?>
@extends('comum')

@section('corpo')

<section class="pagina">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="titulo">{{ $pagina }}</h1>
				<ul class="depo">
					@foreach($depoimentos as $depoimento)
						<li>
							<div class="depoimento">
								<div class="dep-cliente">{{ $depoimento->nomeCliente }}</div>
								{{ $depoimento->depoimento }}
								<div class="estrelas"></div>
							</div>
						</li>
					@endforeach
				</ul>
			</div>
		</div>
	</div>
</section>
@endsection