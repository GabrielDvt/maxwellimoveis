@extends('painel.modeloPainel')

@section('titulo')
{{ "Serviços" }}
@endsection

<!-- MODAIS -->
@include('painel.inc.modal.servicos.adicionar')
@include('painel.inc.modal.servicos.editar')
@include('painel.inc.modal.padrao.deletar')

@section('tabela')

        <table class="table table-striped">
            <thead>
                <th>Nome do serviço</th>
                <th>Descrição</th>
                <th>Imagem</th>
                <th class="w100"></th>
            </thead>

            <tbody>
                @foreach($servicos as $servico)
                    <tr>
                        <td>{{ $servico->nome }}</td>
                        <td>{!! $servico->descricao !!}</td>
                        <td><img class="img-thumbnail" height="200" width="200" src="{{ asset('storage/app/public/' . $servico->urlImagem) }}"></td>
                        
                        <td class="w100 direita">
                            <button id="edit" data-obj="{{ $servico }}" data-toggle="modal" data-target="#editar" type="button" class="btn btn-laranja btn-xs edit"><i class="fa fa-pencil"></i></button>
                            
                            {!! Form::open(['method' => 'DELETE', 'route' => ['servicos.destroy', $servico->id], 'style'=>'display:inline', 'class' => 'delete', 'onSubmit' => 'return confirmDelete()']) !!}
                                {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'class="btn btn-laranja btn-xs"']) !!} 
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    
@endsection

@section('scripts')
    {!! Html::script('js/painel/servicos.js') !!}
@endsection