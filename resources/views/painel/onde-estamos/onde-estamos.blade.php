<?php $pagina = "Onde Estamos"; ?>
@extends('painel.modeloPainel')

@section('titulo')
{{ "Onde estamos" }}
@endsection

<!-- MODAIS -->
@include('painel.inc.modal.onde-estamos.adicionar')
@include('painel.inc.modal.onde-estamos.editar')
@include('painel.inc.modal.padrao.deletar')


@section('tabela')
    <table class="table table-striped">
        <thead>
            <th>Unidade</th>
            <th>Telefone</th>
            <th>Whatsapp</th>
            <th class="w100"></th>
        </thead>

        <tbody>
            @foreach($localizacoes as $localizacao)
                <tr>
                    <td> {{ $localizacao->unidade }} </td>
                    <td> {{ $localizacao->telefone }} </td>
                    <td> {{ $localizacao->whatsapp }} </td>
                    <td class="w100 direita">
                        <button id="editLocalizacao" data-obj="{{ $localizacao }}" data-toggle="modal" data-target="#editar" type="button" class="btn btn-laranja btn-xs edit"><i class="fa fa-pencil"></i></button>
                        
                        {!! Form::open(['method' => 'DELETE', 'route' => ['onde-estamos.destroy', $localizacao->id], 'style'=>'display:inline', 'class' => 'delete', 'onSubmit' => 'return confirmDelete()']) !!}
                            {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'class="btn btn-laranja btn-xs"']) !!} 
                        {!! Form::close() !!}
                    </td>   
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection

@section('scripts')
    {!! Html::script('js/painel/onde-estamos.js') !!}
@endsection
