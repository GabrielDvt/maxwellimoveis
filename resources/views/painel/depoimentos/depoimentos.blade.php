<?php $pagina = "Depoimentos"; ?> 
@extends('painel.modeloPainel') 
@include('painel.inc.header')

@section('titulo')
	Depoimentos 
@endsection

<!-- MODAIS -->
@include('painel.inc.modal.depoimentos.adicionar')
@include('painel.inc.modal.depoimentos.editar')
@include('painel.inc.modal.padrao.deletar')
@section('tabela')

<table class="table table-striped">
	<thead>
		<th>Cliente</th>
		<th>Depoimento</th>
		<th class="w100"></th>
	</thead>

	<tbody>
		@foreach($depoimentos as $depoimento)
		<tr>
			<td> {{ $depoimento->nomeCliente }} </td>
			<td>{!! $depoimento->depoimento !!}</td>
			<td class="w100 direita">

				<button id="edit" data-toggle="modal" data-target="#editar" data-obj="{{ $depoimento }}" type="button" class="btn btn-laranja btn-xs edit">
					<i class="fa fa-pencil"></i>
				</button>

				{!! Form::open(['method' => 'DELETE', 'route' => ['depoimentos.destroy', $depoimento->id], 'style'=>'display:inline', 'class' =>
				'delete', 'onSubmit' => 'return confirmDelete()']) !!} {!! Form::button('
				<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'class="btn btn-laranja btn-xs"']) !!} {!! Form::close() !!}
			</td>
		</tr>
		@endforeach
	</tbody>
</table>
@endsection

@include('painel.inc.footer') 

@section('scripts')
    {!! Html::script('js/painel/depoimentos.js') !!} 
@endsection