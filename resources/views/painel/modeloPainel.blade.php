@extends('painel.comum')

@section('corpo')

    <div class="wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet">
                        <div class="portlet-heading bg-inverse">
                            <h3 class="portlet-title">
                                @yield('titulo')
                            </h3>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>

            @if(Session::has('success'))
                <div class="alert alert-info">
                    {{ Session::get('success') }}
                </div>
            @endif

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="row">
                <div class="col-md-12">
                    <div class="card-box">
                        <div class="table-wrapper">
                            <div class="btn-toolbar bot20">
                                <a href="" data-toggle="modal" data-target="#adicionar" type="button" class="btn btn-laranja">Adicionar</a>
                            </div>

                            <div class="table-responsive">
                                @yield('tabela')
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection




