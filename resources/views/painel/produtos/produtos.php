<?php $pagina = "Produtos"; ?>
<?php include "inc/header.php"; ?>

<div class="wrapper">
    <?php include "inc/botaoUser.php"; ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet">
                    <div class="portlet-heading bg-inverse">
                        <h3 class="portlet-title">
                            <?php echo $pagina ?>
                        </h3>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>

        <!-- MODAIS -->
        <?php include "inc/modal/produtos/adicionar.php"; ?>
        <?php include "inc/modal/produtos/editar.php"; ?>
        <?php include "inc/modal/padrao/deletar.php"; ?>



        <div class="row">
            <div class="col-md-12">
                <div class="card-box">
                    <div class="table-wrapper">
                        <div class="btn-toolbar bot20">
                            <a href="" data-toggle="modal" data-target="#adicionar" type="button" class="btn btn-laranja">Adicionar</a>
                        </div>

                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <th class="w200">Nome do produto</th>
                                    <th class="w200">Categoria</th>
                                    <th class="w100"></th>
                                </thead>

                                <tbody>
                                    <td>lorem</td>
                                    <td>lorem</td>
                                    <td class="w100 direita"><a href="enviar-galeria" type="button" class="btn btn-laranja btn-xs"><i class="fa fa-image"></i></a> <a href="" data-toggle="modal" data-target="#editar" type="button" class="btn btn-laranja btn-xs"><i class="fa fa-pencil"></i></a> <a href="" data-toggle="modal" data-target="#deletar" type="button" class="btn btn-laranja btn-xs"><i class="fa fa-trash"></i></a></td>
                              </tbody>
                          </table>
                      </div>

                  </div>

              </div>
          </div>
      </div>
      <?php include "inc/footer.php"; ?>