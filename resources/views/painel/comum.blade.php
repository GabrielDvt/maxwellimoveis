<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>Painel de controle</title>

	<link rel="stylesheet" href="{{ asset('/painel/assets/plugins/morris/morris.css') }}">
	<link rel="shortcut icon" type="image/png" href="{{ asset('/painel/img/favicon.png') }}">
	<link href="{{ asset('/painel/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/painel/assets/css/alt.css') }}" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
	<link href="{{ asset('/painel/assets/css/core.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/painel/assets/css/components.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/painel/assets/css/icons.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/painel/assets/css/pages.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/painel/assets/css/menu.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/painel/assets/css/responsive.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/painel/assets/plugins/switchery/css/switchery.min.css') }}" rel="stylesheet" />
	<link href="{{ asset('/painel/assets/plugins/summernote/summernote.css') }}" rel="stylesheet" />
	<link href="{{ asset('/painel/assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/painel/assets/plugins/dropzone/dropzone.css') }}" rel="stylesheet" type="text/css" />

	<script src="{{ asset('/painel/assets/js/modernizr.min.js') }}"></script>

</head>

<body>

	<!-- Navigation Bar-->
	<header id="topnav">
		<div class="topbar-main">
			<div class="container">
				<div class="logo bot10">
					<a href="./">
						<img src="{{ asset('/painel/img/logo.png') }}" class="img-responsive hidden-xs" alt="">
					</a>
					<a href="./">
						<img src="{{ asset('/painel/img/logoMobile.png') }}" class="img-responsive visible-xs logoMobile" alt="">
					</a>
				</div>

				<div class="menu-extras">
					<ul class="nav navbar-nav navbar-right pull-right">
						<li class="dropdown navbar-c-items">
							<div class="btn-group" style="margin-top: 15px;">
								<button type="button" class="btn btn-laranja dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">
									<i class="fa fa-user"></i>
									<span class="hidden-xs">Olá {{ $user->email }}</span>
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu" role="menu">
									<li>
										{!! Form::open(array('route' => 'logout', 'method' => 'POST'), ['class' => 'fa fa-angle-right text-custom m-r-10']) !!} {!!
										csrf_field() !!}
										<input type="submit" value="Sair"> {!! Form::close() !!}
									</li>
								</ul>
							</div>
						</li>
					</ul>
					<div class="menu-item">
						<!-- Mobile menu toggle-->
						<a class="navbar-toggle">
							<div class="lines">
								<span></span>
								<span></span>
								<span></span>
							</div>
						</a>
						<!-- End mobile menu toggle-->
					</div>
				</div>

			</div>
		</div>

		<div class="navbar-custom">
			<div class="container">
				<div id="navigation">
					<!-- Navigation Menu-->
					<ul class="navigation-menu">

						<li>
							<a href="{{ route('main') }}">
								<i class="fa fa-home"></i>Início
							</a>
						</li>

						<li>
							<a href="{{ route('institucional.index') }}">
								<i class="fa fa-building"></i> Institucional</a>
						</li>

						<li>
							<a href="{{ route('simulacoes.index') }}">
								<i class="fa fa-calculator"></i> Simulações </a>
						</li>

						<li>
							<a href="{{ route('slides.index') }}">
								<i class="fa fa-image"></i> Slides</a>
						</li>

						<li class="has-submenu">
							<a href="{{ route('ver-imoveis.index') }}">
								<i class="fa fa-home"></i>Imóveis</a>
							<ul class="submenu">
								<li>
									<a href="{{ route('ver-imoveis.index') }}">Ver imóveis</a>
								</li>

								<li>
									<a href="{{ route('tipos.index') }}">Tipos</a>
								</li>

								<li>
									<a href="{{ route('atributos.index') }}">Atributos</a>
								</li>
							</ul>
						</li>

						<li>
							<a href="{{ route('servicos.index') }}">
								<i class="fa fa-wrench" aria-hidden="true"></i>Serviços</a>
						</li>

						<li>
							<a href="{{ route('depoimentos.index') }}">
								<i class="fa fa-comments" aria-hidden="true"></i>Depoimentos</a>
						</li>

						<li>
							<a href="{{ route('onde-estamos.index') }}">
								<i class="fa fa-map-marker" aria-hidden="true"></i>Onde Estamos</a>
						</li>

						<li>
							<a href="{{ route('configuracoes.index') }}">
								<i class="fa fa-cogs"></i>Configurações</a>
						</li>
						
					</ul>
				</div>
			</div>
		</div>
	</header>

	@include('painel.inc.modal.usuarios.alterar-senha')

	<!-- Aqui vem o conteúdo -->

	@yield('corpo')

	<script src="{{ asset('/painel/assets/js/jquery.min.js') }}"></script>
	<script src="{{ asset('/painel/assets/js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('/painel/assets/js/detect.js') }}"></script>
	<script src="{{ asset('/painel/assets/js/fastclick.js') }}"></script>
	<script src="{{ asset('/painel/assets/js/jquery.slimscroll.js') }}"></script>
	<script src="{{ asset('/painel/assets/js/jquery.blockUI.js') }}"></script>
	<script src="{{ asset('/painel/assets/js/waves.js') }}"></script>
	<script src="{{ asset('/painel/assets/js/wow.min.js') }}"></script>
	<script src="{{ asset('/painel/assets/js/jquery.nicescroll.js') }}"></script>
	<script src="{{ asset('/painel/assets/js/jquery.scrollTo.min.js') }}"></script>
	<script src="{{ asset('/painel/assets/plugins/dropzone/dropzone.js') }}"></script>
	<script src="{{ asset('/js/jquery.mask.js') }}" type="text/javascript"></script>

	<!--Summernote js-->
	<script src="{{ asset('/painel/assets/plugins/summernote/summernote.js') }}"></script>

	<!-- scripts das views -->
	@yield('scripts')

	<!-- App core js -->
	<script src="{{ asset('/painel/assets/js/jquery.core.js') }}"></script>
	<script src="{{ asset('/painel/assets/js/jquery.app.js') }}"></script>

	<script>
		jQuery(document).ready(function(){

                $('.summernote').summernote({
                    height: 250,                 // set editor height
                    minHeight: null,             // set minimum height of editor
                    maxHeight: null,             // set maximum height of editor
                    focus: false,               // set focus to editable area after initializing summernote
                    toolbar: [
                        // [groupName, [list of button]]
                        ['style', ['bold', 'italic', 'underline', 'clear']],
                        ['font', ['strikethrough', 'superscript', 'subscript']],
                        ['fontsize', ['fontsize']],
                        ['color', ['color']],
                        ['para', ['ul', 'ol', 'paragraph']],
                        ['height', ['height']]
                      ]
                });

                $('.inline-editor').summernote({
                    airMode: true
                });

                $('.money').mask('000.000.000.000.000,00', {reverse: true});

            });
	</script>

</body>

</html>