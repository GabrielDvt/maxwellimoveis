@extends('painel.comum')

@section('corpo')

<div class="wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet">
                    <div class="portlet-heading bg-inverse">
                        <h3 class="portlet-title">
                            Institucional
                        </h3>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>

        @if(Session::has('success'))
            <div class="alert alert-info">
                {{ Session::get('success') }}
            </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="row">
            <div class="col-md-12">
                <div class="card-box">
                    {!! Form::open(array('route' => 'institucional.store')) !!}
                        {{ csrf_field() }}    
                        <textarea class="summernote" name="texto" >
                            {{ $obj->texto }}
                        </textarea>
                    <button type="submit" class="btn btn-laranja">Salvar</button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection