<?php $pagina = "Configurações"; ?> 
@extends('painel.comum')

@section('corpo')
<div class="wrapper">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="portlet">
					<div class="portlet-heading bg-inverse">
						<h3 class="portlet-title">
							{{ $pagina }}
						</h3>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>

        @if(Session::has('success'))
			<div class="alert alert-info">
				{{ Session::get('success') }}
			</div>
		@endif

		@if ($errors->any())
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif

		<div class="row">
			<div class="col-md-12">
				<div class="card-box">

                    {!! Form::open(array('route' => 'configuracoes.store', 'method' => 'POST', 'files' => true, 'enctype' => 'multipart/form-data')) !!}
                        {{ csrf_field() }}
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label for="">Nome da empresa</label>
									<input type="text" name="nome" id="" value="{{ $configuracao->nome }}" class="form-control">
								</div>
							</div>

							<div class="col-sm-6">
								<div class="form-group">
									<label for="">Slogan</label>
									<input type="text" name="slogan" id="" value="{{ $configuracao->slogan }}" class="form-control">
								</div>
							</div>
						</div>


						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label for="">Email</label>
									<input type="email" name="email" id="" value="{{ $configuracao->email }}" class="form-control">
								</div>
							</div>

							<div class="col-sm-3">
								<div class="form-group">
									<label for="">Telefone</label>
									<input type="tel" name="telefone" id="" value="{{ $configuracao->telefone }}" class="form-control">
								</div>
							</div>

							<div class="col-sm-3">
								<div class="form-group">
									<label for="">WhatsApp</label>
									<input type="tel" name="whatsapp" id="" value="{{ $configuracao->whatsapp }}" class="form-control">
								</div>
							</div>
						</div>

						<hr>

						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label for="">Descrição para o Google</label>
									<input type="textarea" name="descricaoGoogle" value="{{ $configuracao->descricaoGoogle }}" id="" class="form-control">
								</div>
							</div>

							<div class="col-sm-6">
								<div class="form-group">
									<label for="">Palavras-chave</label>
									<input type="text" name="palavrasChave" id="" value="{{ $configuracao->palavrasChave }}" class="form-control">
								</div>
							</div>

							<div class="col-sm-6">
								<div class="form-group">
									<label for="">Marca d'água</label>
									<input type="file" name="imagem" id="" class="form-control">
								</div>
							</div>

                        </div>
                        
                    {!! Form::submit('Salvar', ['class' => 'btn btn-laranja']) !!}
				</div>
			</div>
		</div>
@endsection
