<?php include "inc/header-2.php"; ?>
<div class="wrapper-page">
    <div class=" card-box">
        <div class="panel-heading"> 
            <img class="img-responsive" src="img/logo-login.png" alt="">
        </div> 


        <div class="panel-body">
            <form class="form-horizontal m-t-20" action="">

                <div class="alert alert-success centro" role="alert"><i class="fa fa-check" aria-hidden="true"></i> <b>Sucesso!</b><br>
                 Sua nova senha foi enviada para o seu e-mail.</div>

                 <div class="alert alert-danger centro" role="alert"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <b>Erro!</b><br>
                     E-mail não encontrado no sistema.</div>

                     <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control" type="email" required="" placeholder="Insira aqui seu e-mail">
                        </div>
                    </div>

                    
                    <div class="form-group text-center m-t-40">
                        <div class="col-xs-12">
                            <button class="btn btn-padrao btn-block text-uppercase waves-effect waves-light" type="submit">Enviar nova senha</button>
                        </div>
                    </div>

                    <div class="form-group m-t-30 m-b-0">
                        <div class="col-sm-12 centro">
                            <a href="login" class="text-dark"><i class="fa fa-angle-left m-r-5"></i> Voltar para o login</a>
                        </div>
                    </div>

                </form> 
                
            </div>   
        </div>                              
        <div class="row">
            <div class="col-sm-12 text-center">
                <p>&copy; <?php echo date('Y') ?> <a href="http://kodev.com.br" target="_blank">Kodev</a> - Todos os direitos reservados.</p>
                
            </div>
        </div>
        
    </div>
