@extends('painel.modeloPainel')

@include('painel.inc.modal.simulacoes.adicionar')
@include('painel.inc.modal.simulacoes.editar')
@include('painel.inc.modal.padrao.deletar')

@section('titulo')
{{ "Simulações" }}
@endsection

@section('tabela')
    <table class="table table-striped">
        <thead>
            <th>Nome</th>
            <th>Link</th>
            <th class="w100"></th>
        </thead>

        <tbody>
            @foreach($simulacoes as $simulacao)
                <tr>
                    <td> {{ $simulacao->nome }} </td>
                    <td> {{ $simulacao->link }} </td>
                    <td class="w100 direita">
                        <button id="edit" data-obj="{{ $simulacao }}" data-toggle="modal" data-target="#editar" type="button" class="btn btn-laranja btn-xs edit"><i class="fa fa-pencil"></i></button>
                        
                        {!! Form::open(['method' => 'DELETE', 'route' => ['simulacoes.destroy', $simulacao->id], 'style'=>'display:inline', 'class' => 'delete', 'onSubmit' => 'return confirmDelete()']) !!}
                            {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'class="btn btn-laranja btn-xs"']) !!} 
                        {!! Form::close() !!}
                    </td>   
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection

@section('scripts')
    {!! Html::script('js/painel/simulacoes.js') !!}
@endsection

