<!-- Modal -->
<div class="modal fade" id="modalImagens" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				
				<div class="row">
					<h2> Capa atual </h2>
					<div class="col-md-4">
						<img class="img-responsive img-thumbnail" id="imagemCapa"></img>
					</div>
				</div>
			</div>


			<h3> Clique em uma imagem abaixo para ser a nova a capa do imóvel </h3>

			<div class="row">
				@foreach($imovel->imagens as $imagem)
					<div class="col-md-4">
						<img class="img-responsive img-thumbnail" onclick="selecionarImagem({{ $imagem->id }}, {{ $imovel->id }});" src="{{ asset('public/storage/' . $imagem->urlImagem) }}"></img>
						<br>
						<button class="btn btn-danger btn-sm" onclick="excluirImagem({{ $imagem->id }}, {{ $imovel->id }});">Excluir</button>
					</div>
				@endforeach
			</div>

		</div>
	</div>
</div>
