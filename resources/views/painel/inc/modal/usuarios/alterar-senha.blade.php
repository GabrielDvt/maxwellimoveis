<!-- Modal -->
<div class="modal fade" id="alterar-senha" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
     <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title" id="myModalLabel">Alterar senha</h4>
    </div>

    <div class="modal-body">
      <div class="alert alert-success" role="alert"><i class="fa fa-check"></i> Sua senha foi alterada com sucesso!</div>
      <div class="alert alert-danger" role="alert"><i class="fa fa-exclamation-triangle"></i> <b>Erro:</b> Sua senha atual está incorreta!</div>
      <div class="alert alert-danger" role="alert"><i class="fa fa-exclamation-triangle"></i> <b>Erro:</b> A nova senha não se coincide. Tente novamente.</div>
      <form action="">
        <div class="row">
          <div class="col-sm-12">
            <div class="form-group">
              <label for="">Senha atual</label>
              <input type="password" required="" name="" id="" class="form-control">
            </div>
          </div>
        </div>


        <div class="row">
          <div class="col-sm-6">
            <div class="form-group">
              <label for="">Nova senha</label>
              <input type="password" required="" name="" id="" class="form-control">
            </div>
          </div>

          <div class="col-sm-6">
            <div class="form-group">
              <label for="">Repetir nova senha</label>
              <input type="password" required="" name="" id="" class="form-control">
            </div>
          </div>
        </div>

        <hr>

        <div class="row">
          <div class="col-sm-12">
            <div class="form-group">
              <button type="button" class="btn btn-success waves-effect waves-light">
               <span class="btn-label"><i class="fa fa-check"></i>
               </span>Alterar senha</button>  <button type="submit" data-dismiss="modal" class="btn btn-danger waves-effect waves-light">
               Cancelar</button>
             </div>
           </div>
         </div>
       </form>
     </div>
   </div>
 </div>
</div>