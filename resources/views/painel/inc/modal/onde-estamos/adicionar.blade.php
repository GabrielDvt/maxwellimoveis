<!-- Modal -->
<div class="modal fade" id="adicionar" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
     <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title" id="myModalLabel">Adicionar</h4>
    </div>

    <div class="modal-body">
      {{ Form::open(array('route' => array('onde-estamos.store'), 'method' => 'POST')) }}
        
        {{ csrf_field() }}
        <div class="row">

          <div class="col-sm-12">
            <div class="form-group">
              <label for="">Nome da unidade</label>
              <input type="text" required="" name="unidade" id="" class="form-control">
            </div>
          </div>

          <div class="col-sm-12">
            <div class="form-group">
              <label for="">Telefone</label>
              <input type="telefone" required="" name="telefone" id="" class="form-control">
            </div>
          </div>

          <div class="col-sm-12">
            <div class="form-group">
              <label for="">WhatsApp</label>
              <input type="telefone" name="whatsapp" id="" class="form-control">
            </div>
          </div>

          <div class="col-sm-12">
            <div class="form-group">
              <label for="">E-mail</label>
              <input type="email" name="email" id="" class="form-control">
            </div>
          </div>

          <div class="col-sm-12">
            <div class="form-group">
              <label for="">Endereço</label>
              <input type="text" name="endereco" id="" class="form-control">
            </div>
          </div>

          <div class="col-sm-12">
            <div class="form-group">
              <label for="">Link do mapa</label>
              <input type="text" name="linkMapa" id="" class="form-control">
            </div>
          </div>

        </div>

        <hr>

        <div class="row">
          <div class="col-sm-12">
            <div class="form-group">
              <button type="submit" class="btn btn-success waves-effect waves-light">Salvar</button> 
             </div>
           </div>
         </div>

       {!! Form::close() !!}
     </div>
   </div>
 </div>
</div>