<!-- Modal -->
<div class="modal fade" id="editar" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
     <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title" id="myModalLabel">Editar</h4>
    </div>

    <div class="modal-body">
      {{ Form::open(array('route' => array('alterarServico'), 'method' => 'POST', 'files' => true)) }}
      
        <div class="row">
          <input type="hidden" name="id"></input>
          <div class="col-sm-6">
            <div class="form-group">
              <label for="">Nome do serviço</label>
              <input type="text" required="" name="nome" id="" class="form-control" value="">
            </div>
          </div>

          <div class="col-sm-6">
            <div class="form-group">
              <label for="">Imagem</label>
              <input type="file" name="imagem" id="imagem" class="form-control" value="">
            </div>
          </div>

        </div>

        <div class="row">
          <div class="col-sm-12">
            <label for="">Descrição do serviço</label>
            <textarea name="descricao" id="summernoteDescricao" cols="30" rows="10" class="form-control summernote" value=""></textarea>
          </div>
        </div>

        <hr>

        <div class="row">
          <div class="col-sm-12">
            <div class="form-group">
              <button type="submit" class="btn btn-success waves-effect waves-light">Salvar</button> 
             </div>
           </div>
         </div>
       {!! Form::close() !!}
     </div>
   </div>
 </div>
</div>