<!-- Modal -->
<div class="modal fade" id="adicionar" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
     <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title" id="myModalLabel">Adicionar</h4>
    </div>

    <div class="modal-body">
        {{ Form::open(array('route' => array('simulacoes.store'), 'method' => 'POST')) }}
        
        {{ csrf_field() }}
        <div class="row">

          <div class="col-sm-12">
            <div class="form-group">
              <label for="">Nome do banco</label>
              <input type="text" required="" name="nome" id="" class="form-control">
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-12">
            <div class="form-group">
                <label for="">Link</label>
                <input type="text" required="" value="http://" name="link" id="" class="form-control">
              </div>
          </div>
        </div>

        <hr>

        <div class="row">
          <div class="col-sm-12">
            <div class="form-group">
              <button type="submit" class="btn btn-success waves-effect waves-light">Salvar</button> 
             </div>
           </div>
         </div>
       {!! Form::close() !!}
     </div>
   </div>
 </div>
</div>