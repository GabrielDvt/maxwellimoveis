<!-- Modal -->
<div class="modal fade" id="editar" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
     <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title" id="myModalLabel">Editar</h4>
    </div>

    <div class="modal-body">
      <form action="">
        <div class="row">
          <div class="col-sm-12">
            <div class="form-group">
              <label for="">Nome do produto</label>
              <input type="text" required="" name="" id="" class="form-control">
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-12">
            <div class="form-group">
              <label for="">Categoria</label>
              <select name="" id="" required="" class="form-control">
                <option value="">-- Selecione uma categoria --</option>
                <option value="">Categoria</option>
                <option value="">|- Subcategoria</option>
                <option value="">Categoria</option>
                <option value="">Categoria</option>
                <option value="">Categoria</option>
              </select>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-12">
            <div class="form-group">
              <label for="">Informações</label>
              <textarea name="" id="" required="" cols="30" rows="10" class="form-control"></textarea>
            </div>
          </div>
        </div>

       

        <hr>

        <div class="row">
          <div class="col-sm-12">
            <div class="form-group">
              <button type="submit" class="btn btn-success waves-effect waves-light">Salvar</button> 
             </div>
           </div>
         </div>
       </form>
     </div>
   </div>
 </div>
</div>