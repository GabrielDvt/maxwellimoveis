<!-- Modal -->
<div class="modal fade" id="adicionar" tabindex="-1" role="dialog" aria-labelledby="">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">Adicionar</h4>
			</div>

			<div class="modal-body">
				{{ Form::open(array('route' => array('tipos.store'), 'method' => 'POST')) }}
				<div class="row">
					<div class="col-sm-12">
						<div class="form-group">
							<label for="">Nome do tipo</label>
							<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
							<input type="text" placeholder="Casa, Apartamento, Sobrado..." required name="nome" id="" class="form-control">
						</div>
					</div>
				</div>

				<hr>

				<div class="row">
					<div class="col-sm-12">
						<div class="form-group">
							<button type="submit" class="btn btn-success waves-effect waves-light">Salvar</button>
						</div>
					</div>
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>