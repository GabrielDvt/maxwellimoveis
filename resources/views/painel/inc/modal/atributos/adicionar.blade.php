<!-- Modal -->
<div class="modal fade" id="adicionar" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
     <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title" id="myModalLabel">Adicionar</h4>
    </div>

    {{ Form::open(array('route' => array('atributos.store'), 'method' => 'POST')) }}
      <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <label for="">Nome do atributo</label>
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <input type="text" required="" name="nome" id="" class="form-control"  placeholder="Ex: Espaço Gourmet, Brinquedoteca, Salão de Festas, etc.">
              </div>
            </div>
          </div>

          <hr>

          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <input type="submit" class="btn btn-laranja" value="Salvar">
              </div>
            </div>
          </div>
      {!! Form::close() !!}
     </div>
   </div>
 </div>
</div>