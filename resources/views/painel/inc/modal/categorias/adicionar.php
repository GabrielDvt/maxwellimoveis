<!-- Modal -->
<div class="modal fade" id="adicionar" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
     <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title" id="myModalLabel">Adicionar</h4>
    </div>

    <div class="modal-body">
      <form action="">
        <div class="row">
          <div class="col-sm-12">
            <div class="form-group">
              <label for="">Nome da categoria</label>
              <input type="text" required="" name="" id="" class="form-control">

              <select name="" id="" class="form-control top10">
                <option value="">** Categoria pai **</option>
                <option value="">Categoria 1</option>
                <option value="">Categoria 2</option>
                <option value="">Categoria 3</option>
              </select>
            </div>
          </div>
        </div>

        <hr>

        <div class="row">
          <div class="col-sm-12">
            <div class="form-group">
              <button type="submit" class="btn btn-success waves-effect waves-light">Salvar</button> 
             </div>
           </div>
         </div>
       </form>
     </div>
   </div>
 </div>
</div>