<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="/painel/assets/images/favicon_1.ico">
        <title>Painel de Controle</title>

		<link rel="stylesheet" href="/painel/assets/plugins/morris/morris.css">
        <link rel="shortcut icon" type="image/png" href="/painel/img/favicon.png">
        <link href="/painel/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="/painel/assets/css/alt.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
        <link href="/painel/assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="/painel/assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="/painel/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="/painel/assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="/painel/assets/css/menu.css" rel="stylesheet" type="text/css" />
        <link href="/painel/assets/css/responsive.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="/painel/assets/js/modernizr.min.js"></script>

    </head>


    <body>
