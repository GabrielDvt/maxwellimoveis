
            </div>
        </div>
        
        <script src="/painel/assets/js/jquery.min.js"></script>
        <script src="/painel/assets/js/bootstrap.min.js"></script>
        <script src="/painel/assets/js/detect.js"></script>
        <script src="/painel/assets/js/fastclick.js"></script>
        <script src="/painel/assets/js/jquery.slimscroll.js"></script>
        <script src="/painel/assets/js/jquery.blockUI.js"></script>
        <script src="/painel/assets/js/waves.js"></script>
        <script src="/painel/assets/js/wow.min.js"></script>
        <script src="/painel/assets/js/jquery.nicescroll.js"></script>
        <script src="/painel/assets/js/jquery.scrollTo.min.js"></script>
        <script src="/painel/assets/plugins/dropzone/dropzone.js"></script>

        <!--Summernote js-->
        <script src="/painel/assets/plugins/summernote/summernote.min.js"></script>

        <!-- App core js -->
        <script src="/painel/assets/js/jquery.core.js"></script>
        <script src="/painel/assets/js/jquery.app.js"></script>

        <script>

            jQuery(document).ready(function(){

                $('.summernote').summernote({
                    height: 250,                 // set editor height
                    minHeight: null,             // set minimum height of editor
                    maxHeight: null,             // set maximum height of editor
                    focus: false                 // set focus to editable area after initializing summernote
                });

                $('.inline-editor').summernote({
                    airMode: true
                });

            });

            $(document).on("click", "#editAtributo", function () {
                var id = $(this).data('id');
                var nome = $(this).data('nome');
                $("#id").val(id);
                $("#nome").val(nome);
            });
        </script>

    </body>
</html>