<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Painel de controle</title>

        <link rel="stylesheet" href="/painel/assets/plugins/morris/morris.css">
        <link rel="shortcut icon" type="image/png" href="/painel/img/favicon.png">
        <link href="/painel/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="/painel/assets/css/alt.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
        <link href="/painel/assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="/painel/assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="/painel/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="/painel/assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="/painel/assets/css/menu.css" rel="stylesheet" type="text/css" />
        <link href="/painel/assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <link href="/painel/assets/plugins/switchery/css/switchery.min.css" rel="stylesheet" />
        <link href="/painel/assets/plugins/summernote/summernote.css" rel="stylesheet" />
        <link href="/painel/assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="/painel/assets/plugins/dropzone/dropzone.css" rel="stylesheet" type="text/css" />


        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
            <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
            <![endif]-->
        <script src="/painel/assets/js/modernizr.min.js"></script>
    </head>

    <body>
        <!-- Navigation Bar-->
        <header id="topnav">
            <div class="topbar-main">
                <div class="container">

                    <div class="logo bot10">
                     <a href="./"><img src="/painel/img/logo.png" class="img-responsive hidden-xs" alt=""></a>
                     <a href="./"><img src="/painel/img/logoMobile.png" class="img-responsive visible-xs logoMobile" alt=""></a>
                 </div>


                 <div class="menu-extras">
                    <ul class="nav navbar-nav navbar-right pull-right">
                        <li class="dropdown navbar-c-items">
                            <div class="btn-group" style="margin-top: 15px;">
                              <button type="button" class="btn btn-laranja dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-user"></i> <span class="hidden-xs">Olá Fulano</span> <span class="caret"></span></button>
                              <ul class="dropdown-menu" role="menu">
                                <li><a href="" data-toggle="modal" data-target="#alterar-senha"><i class="fa fa-angle-right text-custom m-r-10"></i> Alterar senha</a></li>
                                <li><a href="#"><i class="fa fa-angle-right text-custom m-r-10"></i> Sair</a></li>
                            </ul>
                        </div>
                    </li>
                </ul>
                <div class="menu-item">
                    <!-- Mobile menu toggle-->
                    <a class="navbar-toggle">
                        <div class="lines">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </a>
                    <!-- End mobile menu toggle-->
                </div>
            </div>

            

        </div>
    </div>

    <div class="navbar-custom">
        <div class="container">
            <div id="navigation">
                <!-- Navigation Menu-->
                <ul class="navigation-menu">

                    <li><a href="./"><i class="fa fa-home"></i>Início</a></li>

                    <li><a href="/administrador/institucional"><i class="fa fa-building"></i> Institucional</a></li>

                    <li><a href="/administrador/slides"><i class="fa fa-image"></i> Slides</a></li>

                    <li class="has-submenu">
                        <a href="#"><i class="fa fa-home"></i>Imóveis</a>
                        <ul class="submenu">
                            <li><a href="/administrador/ver-imoveis">Ver imóveis</a></li>
                            <li><a href="/administrador/tipos">Tipos</a></li>
                            <li><a href="/administrador/atributos">Atributos</a></li>
                        </ul>
                    </li>

                    <li><a href="/administrador/servicos"><i class="fa fa-wrench" aria-hidden="true"></i>Serviços</a></li>
                    <li><a href="/administrador/depoimentos"><i class="fa fa-comments" aria-hidden="true"></i>Depoimentos</a></li>
                    <li><a href="/administrador/onde-estamos"><i class="fa fa-map-marker" aria-hidden="true"></i>Onde Estamos</a></li>
                    <li><a href="/administrador/configuracoes"><i class="fa fa-cogs"></i>Configurações</a></li>
                </ul>
            </div>
        </div> 
    </div> 


</header>

@include('painel.inc.modal.usuarios.alterar-senha')

