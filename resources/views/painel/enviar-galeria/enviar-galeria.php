<?php $pagina = "Galeria de fotos"; ?>
<?php include "inc/header.php"; ?>

<div class="wrapper">
    <?php include "inc/botaoUser.php"; ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet">
                    <div class="portlet-heading bg-inverse">
                        <h3 class="portlet-title">
                            <?php echo $pagina ?>
                        </h3>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 portlets">
                <!-- Your awesome content goes here -->
                <div class="m-b-30">
                    <form action="#" class="dropzone" id="dropzone">
                      <div class="fallback">
                        <input name="file" type="file" multiple />
                    </div>

                </form>
                <button type="button" class="btn btn-laranja top20">Salvar</button>
            </div>
        </div>
    </div>
    <?php include "inc/footer.php"; ?>