@extends('painel.modeloPainel')

@section('titulo')
{{ "Slides" }}
@endsection

@include('painel.inc.modal.slide.adicionar')
@include('painel.inc.modal.slide.editar')
@include('painel.inc.modal.slide.verImagem')
@include('painel.inc.modal.padrao.deletar')

@section('tabela')

<table class="table table-striped">
    <thead>
        <th>Nome do banner</th>
        <th class="w100"></th>
    </thead>

    <tbody>
        @foreach($slides as $slide)
            <tr>
                <td>{{ $slide->nome }}</td>
                <td class="w100 direita">

                    <button id="verImagem" data-url="{{ asset('public/storage/' . $slide->urlImagem) }}" data-toggle="modal" data-target="#verImagemModal" type="button" class="btn btn-laranja btn-xs edit">Ver imagem</button>

                    <button id="edit" data-obj="{{ $slide }}" data-toggle="modal" data-target="#editar" type="button" class="btn btn-laranja btn-xs edit"><i class="fa fa-pencil"></i></button>
                    
                    {!! Form::open(['method' => 'DELETE', 'route' => ['slides.destroy', $slide->id], 'style'=>'display:inline', 'class' => 'delete', 'onSubmit' => 'return confirmDelete()']) !!}
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'class="btn btn-laranja btn-xs"']) !!} 
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

@endsection

@section('scripts')
{!! Html::script('js/painel/slides.js') !!}
@endsection