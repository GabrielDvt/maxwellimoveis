@extends('painel.modeloPainel')

@include('painel.inc.modal.atributos.adicionar')
@include('painel.inc.modal.atributos.editar')
@include('painel.inc.modal.padrao.deletar')

@section('titulo')
{{ "Atributos" }}
@endsection

@section('tabela')
    <table class="table table-striped">
        <thead>
            <th>Título</th>
            <th class="w100"></th>
        </thead>

        <tbody>
            @foreach($atributos as $atributo)
                <tr>
                    <td> {{ $atributo->nome }} </td>
                    <td class="w100 direita">
                    
                        <button id="editAtributo" data-toggle="modal" data-target="#editar" data-id="{{ $atributo->id }}" data-nome="{{ $atributo->nome }}" type="button" class="btn btn-laranja btn-xs edit"><i class="fa fa-pencil"></i></button>
                        
                        {!! Form::open(['method' => 'DELETE', 'route' => ['atributos.destroy', $atributo->id], 'style'=>'display:inline', 'class' => 'delete', 'onSubmit' => 'return confirmDelete()']) !!}
                            {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'class="btn btn-laranja btn-xs"']) !!} 
                        {!! Form::close() !!}
                    </td>   
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection

@section('scripts')
    {!! Html::script('js/painel/atributos.js') !!}
@endsection
