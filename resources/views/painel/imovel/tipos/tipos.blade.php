@extends('painel.modeloPainel')

@include('painel.inc.modal.tipos.adicionar')
@include('painel.inc.modal.tipos.editar')
@include('painel.inc.modal.padrao.deletar')

@section('titulo')
{{ "Tipos" }}
@endsection

@section('tabela')
    <table class="table table-striped">
        <thead>
            <th>Título</th>
            <th class="w100"></th>
        </thead>

        <tbody>
            @foreach($tipos as $tipo)
                <tr>
                    <td> {{ $tipo->nome }} </td>
                    <td class="w100 direita">
                        
                        <button id="editTipo" data-toggle="modal" data-target="#editar" data-id="{{ $tipo->id }}" data-nome="{{ $tipo->nome }}" type="button" class="btn btn-laranja btn-xs edit"><i class="fa fa-pencil"></i></button>
                        
                        {!! Form::open(['method' => 'DELETE', 'route' => ['tipos.destroy', $tipo->id], 'style'=>'display:inline', 'class' => 'delete', 'onSubmit' => 'return confirmDelete()']) !!}
                            {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'class="btn btn-laranja btn-xs"']) !!} 
                        {!! Form::close() !!}
                    </td>   
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection

@section('scripts')
    {!! Html::script('js/painel/tipos.js') !!}
@endsection
