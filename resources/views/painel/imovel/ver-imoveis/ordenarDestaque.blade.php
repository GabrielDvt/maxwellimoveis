@extends('painel.comum')

@section('corpo')

    <div class="wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet">
                        <div class="portlet-heading bg-inverse">
                            <h3 class="portlet-title">
                                Ordenar Destaque
                            </h3>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>

            @if(Session::has('success'))
                <div class="alert alert-info">
                    {{ Session::get('success') }}
                </div>
            @endif

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="row">
                <div class="col-md-12">
                    <div class="card-box">
                        <div class="table-wrapper">
                           
                            <div class="table-responsive">
                                   
                                <table class="table table-striped">
                                    <thead>
                                        <th class="centro">Ref.</th>
                                        <th>Nome</th>
                                        <th class="centro">Valor</th>
                                        <th class="centro">Finalidade</th>
                                        <th class="w100"></th>
                                        <th class="w100"></th>
                                    </thead>

                                    <tbody id="body">
                                        @foreach($imoveis as $imovel)
                                            <tr>
                                                {!! Form::open(['method' => 'PUT', 'route' => ['alterarPosicaoDestaque'], 'style'=>'display:inline', 'class' => 'delete']) !!}
                                                    <input type="hidden" name="id" value="{{ $imovel->id }}">
                                                    <td class="centro ordernar-id">{{ $imovel->id }}</td>
                                                    <td>{{ $imovel->nome }}</td>
                                                    <td class="centro">R$ {{ number_format($imovel->valor, 2, ',', '') }}</td>
                                                    <td class="centro">@if($imovel->finalidade == 1) Imóvel à venda @else Imóvel para aluguel @endif</td>
                                                    <td class="centro"><input type="text" value="{{ $imovel->posicaoDestaque }}" style="width: 100px" name="posicaoDestaque" class="form-control" placeholder="Posição"></td>
                                                    <td>
                                                        {!! Form::button('Alterar', ['type' => 'submit', 'class' => 'btn btn-laranja']) !!} 
                                                {!! Form::close() !!}
                                                </td>
                                            </tr>
                                        @endforeach
                                        {{-- <button class="btn btn-laranja" id="btnAtualizar">Atualizar</button> --}}
                                    </tbody>
                                </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection

@section('scripts')
    {!! Html::script('js/painel/ver-imoveis.js') !!}
@endsection


