@extends('painel.comum')

@section('corpo')

    <div class="wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet">
                        <div class="portlet-heading bg-inverse">
                            <h3 class="portlet-title">
                                Imóveis
                            </h3>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>

            @if(Session::has('success'))
                <div class="alert alert-info">
                    {{ Session::get('success') }}
                </div>
            @endif

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="row">
                <div class="col-md-12">
                    <div class="card-box">
                        <div class="table-wrapper">
                            <div class="btn-toolbar bot20">
                                {!! link_to_route('ver-imoveis.create',"Adicionar", [], ['class' => 'btn btn-laranja']) !!}
                            </div>

                            <div class="btn-toolbar bot20">
                                {!! link_to_route('ordenarDestaques',"Ordenar Destaques", [], ['class' => 'btn btn-default']) !!}
                            </div>

                            <div class="table-responsive">
                                   
                                <table class="table table-striped">
                                    <thead>
                                        <th class="centro">Ref.</th>
                                        <th>Nome</th>
                                        <th class="centro">Valor</th>
                                        <th class="centro">Finalidade</th>
                                        <th class="centro">Destaque</th>
                                        <th class="centro">Status</th>
                                        <th class="centro">Vendido</th>
                                        <th class="w100"></th>
                                    </thead>

                                    <tbody>
                                        @foreach($imoveis as $imovel)
                                        <tr>
                                            <td class="centro">{{ $imovel->id }}</td>
                                            <td>{{ $imovel->nome }}</td>
                                            <td class="centro">R$ {{ number_format($imovel->valor, 2, ',', '') }}</td>
                                            <td class="centro">@if($imovel->finalidade == 1) Imóvel à venda @else Imóvel para aluguel @endif</td>
                                            <td class="centro">
                                                <a href="{{ route('alterar-destaque', $imovel->id) }}"><img src= @if($imovel->destaque) {{ asset('painel/img/1.png') }} @else {{ asset('painel/img/0.png') }} @endif height="12" width="12" alt=""></a>
                                            </td>
                                            
                                            <td class="centro">
                                                <a href="{{ route('alterar-status', $imovel->id) }}"><img src= @if($imovel->status) {{ asset('painel/img/1.png') }} @else {{ asset('painel/img/0.png') }} @endif height="12" width="12" alt=""></a>
                                            </td>

                                            <td class="centro">
                                                <a href="{{ route('alterar-vendido', $imovel->id) }}"><img src= @if($imovel->vendido) {{ asset('painel/img/1.png') }} @else {{ asset('painel/img/0.png') }} @endif height="12" width="12" alt=""></a>
                                            </td>

                                            <td class="w100 direita">
                                                
                                                <a href="{{ route('ver-imoveis.edit', $imovel->id) }}"  type="button" class="btn btn-laranja btn-xs"><i class="fa fa-pencil"></i></a> 
                                                {!! Form::open(['method' => 'DELETE', 'route' => ['ver-imoveis.destroy', $imovel->id], 'style'=>'display:inline', 'class' => 'delete', 'onSubmit' => 'return confirmDelete()']) !!}
                                                {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'class="btn btn-laranja btn-xs"']) !!} 
                                                {!! Form::close() !!}
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection

@section('scripts')
    {!! Html::script('js/painel/ver-imoveis.js') !!}
@endsection


