<?php $pagina = "Adicionar Imóvel"; ?> @extends('painel.comum')

<div class="wrapper">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="portlet">
					<div class="portlet-heading bg-inverse">
						<h3 class="portlet-title">
							{{ $pagina }}
						</h3>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>

		@if(Session::has('success'))
			<div class="alert alert-info">
				{{ Session::get('success') }}
			</div>
		@endif

		@if ($errors->any())
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif

		{!! Form::open(array('route' => 'ver-imoveis.store', 'method' => 'POST', 'files' => true, 'enctype' => 'multipart/form-data', 'id' => 'formulario')) !!}
	    {{ csrf_field() }}

		<div class="row">
			<div class="col-md-12">
				<div class="card-box">
					<div class="row bot20">
						<div class="col-sm-4">
							<div class="tituloForm">Nome do imóvel</div>
							<input type="text" name="nome" id="" value="{{ old('nome') }}" class="form-control" required="">
						</div>

						<div class="col-sm-4">
							<div class="tituloForm">Código do Imóvel</div>
							<input type="text" name="referencia" id="" value="{{ old('referencia') }}" class="form-control" required="">
						</div>
					</div>

					<div class="row bot20">
						<div class="col-sm-3">
							<div class="tituloForm">Valor</div>
							<input type="text" name="valor" required="" id="valor" value="{{ old('valor') }}"  class="form-control">
						</div>

						<div class="col-sm-3">
							<div class="tituloForm">Finalidade</div>
							<select name="finalidade" id="" class="form-control" value="{{ old('finalidade') }}">
								<option value="">-- Selecione --</option>
								<option value="1">Imóvel para venda</option>
								<option value="2">Imóvel para aluguel</option>
							</select>
						</div>

						<div class="col-sm-3">
							<div class="tituloForm">Tipo</div>
							<select name="tipo_id" id="" class="form-control" value="{{ old('tipo_id') }}">
								<option value="">-- Selecione --</option>
								@foreach($tipos as $tipo)
									<option value="{{$tipo->id}}">{{ucfirst($tipo->nome)}}</option>
								@endforeach
							</select>
						</div>

						<div class="col-sm-3">
							<div class="tituloForm">Área em m²</div>
							<input type="text" name="area" value="{{ old('area') }}" required="" id="" class="form-control">
						</div>
					</div>

					<div class="row bot20">
						<div class="col-sm-3">
							<div class="tituloForm">Quartos</div>
							<input type="number" name="quartos" value="{{ old('quartos') }}" required="" id="" class="form-control">
						</div>

						<div class="col-sm-3">
							<div class="tituloForm">Banheiros</div>
							<input type="number" name="banheiros" value="{{ old('banheiros') }}" required="" id="" class="form-control">
						</div>

						<div class="col-sm-3">
							<div class="tituloForm">Garagens</div>
							<input type="number" name="garagens" value="{{ old('garagens') }}" required="" id="" class="form-control">
						</div>

						<div class="col-sm-3">
							<div class="tituloForm">Suítes</div>
							<input type="number" name="suites" value="{{ old('suites') }}" required="" id="" class="form-control">
						</div>
					</div>

					<div class="row bot20">
						<div class="col-sm-4">
							<div class="tituloForm">Estado</div>
							<select name="estado_id" id="estados" value="{{ old('estados') }}" class="form-control" required="">
								<option value="">-- Selecione --</option>
								@foreach($estados as $estado)
									<option value="{{ $estado->id }}" @if(old('estado_id') == $estado->id) selected @endif>{{ $estado->nome }} ({{ $estado->abreviacao }})</option>
								@endforeach
							</select>
						</div>

						<div class="col-sm-4">
							<div class="tituloForm">Cidade</div>
							<select name="cidade_id" id="cidades" value="{{ old('cidades') }}" class="form-control" required="">
								<option value="">-- Selecione --</option>
							</select>
						</div>

						<div class="col-sm-4">
							<div class="tituloForm">Bairro</div>
							<input type="text" name="bairro" value="{{ old('bairro') }}" required="" id="" class="form-control">
						</div>

					</div>
					<hr>

					<div class="row bot20">
						<div class="col-md-6">
							<div class="tituloForm">Fotos do imóvel</div>
							
							<input type="file" name="imagens[]" multiple="multiple" value="{{ old('imagens') }}" id="" class="form-control">
						</div>

						<div class="col-md-6">
							<div class="tituloForm">Atributos</div>
							<a class="btn btn-laranja" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
								Aplicar atributos
							</a>

							<div class="collapse top20" id="collapseExample">
								<div class="well">
									@foreach($atributos as $atributo)
									<div class="checkbox">
										<label>
											<input type="checkbox" name="atributo_id[]" value="{{ $atributo->id }}">{{ $atributo->nome }}
										</label>
									</div>
									@endforeach
								</div>
							</div>
						</div>
					</div>

					<div class="row bot20">
						<div class="col-md-12">
							<div class="tituloForm">Descrição</div>
							<div class="card-box">
								<textarea class="summernote" name="descricao">
								</textarea>
							</div>
						</div>
					</div>
						
					<hr>
					{!! Form::submit('Salvar', array('class' => 'btn btn-laranja')) !!}
					
				</div>
			</div>
		</div>
		{!! Form::close() !!}
		
		@section('scripts')
		<script>

			$(document).ready(function() {
				if (localStorage.getItem("cidade") !== null) {
					$("#estados").trigger('change');
				}
			});

			// $("#formulario").submit(function() {
			// 	$("#valor").val($('#valor').cleanVal());
			// });

			$("#cidades").on('change', function(e) {
				localStorage.cidade = $(this).val();
			});

			$('#estados').on('change', function(e) {
				var id = e.target.value;
				/* busca as cidades de um determinado estado */
				$.get("{{ url('/administrador/buscar-cidades') }}/" + id, function(data) {
					//esvazia as cidades anteriores	
					$('#cidades').empty();
					$.each(data, function(index,subCatObj){
						var option = new Option(subCatObj.nome, subCatObj.id);
						$(option).html(subCatObj.nome);
						$('#cidades').append(option);
					});
				}).done(function() {
					if (localStorage.getItem("cidade") !== null) {
						console.log(localStorage.cidade);
						$("#cidades").val(localStorage.cidade);
					}
				});
			});
		</script>
		@endsection