<?php $pagina = "Adicionar Imóvel"; ?> 
@extends('painel.comum')
@include('painel.inc.modal.ver-imoveis.imagens')

<div class="wrapper">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="portlet">
					<div class="portlet-heading bg-inverse">
						<h3 class="portlet-title">
							{{ $pagina }}
						</h3>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>

		@if(Session::has('success'))
			<div class="alert alert-info">
				{{ Session::get('success') }}
			</div>
		@endif

		@if ($errors->any())
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif

		{!! Form::open(array('route' => ['ver-imoveis.update', $imovel->id], 'method' => 'PUT', 'files' => true, 'enctype' => 'multipart/form-data')) !!}
	    {{ csrf_field() }}

		<div class="row">
			<div class="col-md-12">
				<div class="card-box">
					<div class="row bot20">
						<div class="col-sm-4">
							<div class="tituloForm">Nome do imóvel</div>
							<input type="text" name="nome" id="" value="{{ $imovel->nome }}" class="form-control" required="">
						</div>

						<div class="col-sm-4">
							<div class="tituloForm">Código do Imóvel</div>
							<input type="text" name="referencia" id="" value="{{ $imovel->referencia }}" class="form-control" required="">
						</div>
					</div>

					<div class="row bot20">
						<div class="col-sm-3">
							<div class="tituloForm">Valor</div>
							<input type="text" name="valor" required="" id="" value="{{ $imovel->valor }}" class="form-control">
						</div>

						<div class="col-sm-3">
							<div class="tituloForm">Finalidade</div>

							{{ Form::select('finalidade', ["" => "-- Selecione --", "1" => "Imóvel à venda", "2" => "Imóvel para aluguel"], $imovel->finalidade, ['class' => 'form-control']) }}

						</div>

						<div class="col-sm-3">
                            <div class="tituloForm">Tipo</div>
							
							<select class="form-control" name="tipo_id">
								<option value="">-- Selecione --</option>
								@foreach($tipos as $tipo)
									<option @if($tipo->id == $imovel->tipo_id) selected @endif value="{{ $tipo->id }}">{{ $tipo->nome }}</option>
								@endforeach
							</select>
                           
						</div>

						<div class="col-sm-3">
							<div class="tituloForm">Área em m²</div>
							<input type="text" name="area" value="{{ $imovel->area }}" required="" id="" class="form-control">
						</div>
					</div>

					<div class="row bot20">
						<div class="col-sm-3">
							<div class="tituloForm">Quartos</div>
							<input type="number" name="quartos" value="{{ $imovel->quartos }}" required="" id="" class="form-control">
						</div>

						<div class="col-sm-3">
							<div class="tituloForm">Banheiros</div>
							<input type="number" name="banheiros" value="{{ $imovel->banheiros }}" required="" id="" class="form-control">
						</div>

						<div class="col-sm-3">
							<div class="tituloForm">Garagens</div>
							<input type="number" name="garagens" value="{{ $imovel->garagens }}" required="" id="" class="form-control">
						</div>

						<div class="col-sm-3">
							<div class="tituloForm">Suítes</div>
							<input type="number" name="suites" value="{{ $imovel->suites }}" required="" id="" class="form-control">
						</div>
					</div>

					<div class="row bot20">
						<div class="col-sm-4">
							<div class="tituloForm">Estado</div>
							<select name="estado_id" id="estados" value="{{ $imovel->estados }}" class="form-control" required="">
								<option value="">-- Selecione --</option>
								@foreach($estados as $estado)
									<option value="{{ $estado->id }}" @if($objEstado->estado_id == $estado->id) selected @endif>{{ $estado->nome }} ({{ $estado->abreviacao }})</option>
								@endforeach
							</select>
						</div>

						<div class="col-sm-4">
							<div class="tituloForm">Cidade</div>
							<select name="cidade_id" id="cidades" value="{{ $imovel->cidades }}" class="form-control" required="">
								<option value="">-- Selecione --</option>
							</select>
						</div>

						<div class="col-sm-4">
							<div class="tituloForm">Bairro</div>
							<input type="text" name="bairro" value="{{ $imovel->bairro }}" required="" id="" class="form-control">
						</div>

					</div>
					<hr>

					<div class="row bot20">
						<div class="col-md-4">
							<div class="tituloForm">Upload das fotos</div>
							<input type="file" name="imagens[]" multiple="multiple" value="{{ $imovel->imagens }}" id="" class="form-control">
						</div>

						<div class="col-md-2">
							<div class="tituloForm">Fotos</div>
							<!-- imagens.blade.php -->
							<a class="btn btn-laranja" role="button" data-toggle="modal" data-target="#modalImagens" aria-expanded="false" aria-controls="collapseExample">
								Definir foto de capa
							</a>
						</div>

						<div class="col-md-6">
							<div class="tituloForm">Atributos</div>
							<a class="btn btn-laranja" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
								Aplicar atributos
							</a>

							<div class="collapse top20" id="collapseExample">
								<div class="well">

									@foreach($intersect as $atributo)
										<div class="checkbox">
											<label>
												<input type="checkbox" checked name="atributo_id[]" value="{{ $atributo->id }}">{{ $atributo->nome }}
											</label>
										</div>
									@endforeach

									@foreach($diff as $atributo)
										<div class="checkbox">
											<label>
												<input type="checkbox" name="atributo_id[]" value="{{ $atributo->id }}">{{ $atributo->nome }}
											</label>
										</div>
									@endforeach
								</div>
							</div>
						</div>
					</div>

					<div class="row bot20">
						<div class="col-md-12">
							<div class="tituloForm">Descrição</div>
							<div class="card-box">
								<textarea class="summernote" name="descricao">
									{!! $imovel->descricao !!}
								</textarea>
							</div>
						</div>
					</div>

					<hr>
					{!! Form::submit('Salvar', array('class' => 'btn btn-laranja')) !!}
					
				</div>
			</div>
		</div>
		{!! Form::close() !!}
		
		@section('scripts')
		<script>

			$(document).ready(function() {
				$("#estados").trigger('change');
				
			});

			$(".imagens").on('click', function(e) {
				console.log(e);
			});

			$('#estados').on('change', function(e) {
				var id = e.target.value;
				/* busca as cidades de um determinado estado */
				$.get("{{ url('/administrador/buscar-cidades') }}/" + id, function(data) {
					//esvazia as cidades anteriores	
					$('#cidades').empty();
					$.each(data, function(index,subCatObj){
						var option = new Option(subCatObj.nome, subCatObj.id);
						$(option).html(subCatObj.nome);
						$('#cidades').append(option);
					});
				}).done(function() {
					$("#cidades").val({{ $cidade_id }}).change();
				});
			});

			/* antes do modal de imagens abrir */
			$('#modalImagens').on('shown.bs.modal', function (e) {
				
				var url = '{{ asset('public/storage/') }}';

				$.ajax({
					type: "GET",
					dataType:'html',
					url : "/administrador/ver-imoveis/buscarCapa/" + {{ $imovel->id }},
					success : function (data) {
						$("#imagemCapa").attr('src', url + "/" + data);
					}
				});

			});

			//seleciona uma imagem e a define como a capa do imóvel
			function selecionarImagem(idImagem, idImovel) {
				
				var json = {'_token': '{{ csrf_token() }}', 'imagem_id': idImagem, 'imovel_id': idImovel};
				
				$.ajax({
					type: "POST",
					dataType:'html',
					data: json,
					url : "/administrador/ver-imoveis/alterarCapa",
					success : function (data) {
						$("#modalImagens").modal('toggle');
					}
				});
			}

			function excluirImagem(idImagem, idImovel) {
				$.ajax({
			          type: "POST",
			          data: {'_token': "{{ csrf_token() }}", 'imagem_id': idImagem, 'imovel_id': idImovel},
			          url: "/administrador/ver-imoveis/excluirImagem",
			          success: function(s) {
			             location.reload();
			          }	
			      });
			}
		</script>
		@endsection