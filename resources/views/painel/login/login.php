<?php include "inc/header-2.php"; ?>
<div class="wrapper-page">
    <div class="card-box">
        <div class="panel-heading"> 
            <img class="img-responsive" src="img/logo-login.png" alt="">
        </div> 


        <div class="panel-body">
            <form class="form-horizontal m-t-20" action="">

                <div class="alert alert-danger centro" role="alert"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <b>Falha ao acessar</b> <br>
                   E-mail e/ou senha incorreto(s).</div>

                   <div class="form-group ">
                    <div class="col-xs-12">
                        <input class="form-control" type="email" required="" placeholder="E-mail">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-12">
                        <input class="form-control" type="password" required="" placeholder="Senha">
                    </div>
                </div>

              
                
                <div class="form-group text-center m-t-40">
                    <div class="col-xs-12">
                        <button class="btn btn-padrao btn-block text-uppercase waves-effect waves-light" type="submit">Entrar</button>
                    </div>
                </div>

                <div class="form-group m-t-30 m-b-0">
                    <div class="col-sm-12 centro">
                        <a href="recuperar-senha" class="text-dark"><i class="fa fa-lock m-r-5"></i> Esqueceu sua senha?</a>
                    </div>
                </div>
            </form> 
            
        </div>   
    </div>                              
    <div class="row">
        <div class="col-sm-12 text-center">
            <p>&copy; <?php echo date('Y') ?> <a href="http://kodev.com.br" target="_blank">Kodev</a> - Todos os direitos reservados.</p>
            
        </div>
    </div>
    
</div>
