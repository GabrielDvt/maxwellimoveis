{{-- Este é o layout do e-mail enviado pela aba 'contato' --}}
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>

	<h1>Solicitação para ligação</h1>
	<p>Nome: {{ $contato->nome }}</p>
	<p>E-Mail: {{ $contato->email }}</p>
	<p>Telefone: {{ $contato->ddd }} - {{ $contato->telefone }}</p>
	<p>Horário: {{ $contato->horario }}</p>

</body>
</html>