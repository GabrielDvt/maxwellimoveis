<!DOCTYPE html>
<html>
<head>
	<title>Dúvida Sobre o Imóvel {{ $idImovel }}</title>
</head>
<body>

	<h1>Dúvida sobre o imóvel {{$idImovel}}</h1>
	<p>Referência do imóvel: {{ $idImovel }}</p>
	<p>Nome do imóvel: {{ $nomeImovel }}</p>
	<p>Nome: {{ $contato->nome }}</p>
	<p>E-Mail: {{ $contato->email }}</p>
	<p>Telefone: {{ $contato->ddd }} - {{ $contato->telefone }}</p>
	<p>Dúvida: {{ $contato->duvida }}</p>

</body>
</html>