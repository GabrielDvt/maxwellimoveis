{{-- Este é o layout do e-mail enviado pela aba 'contato' --}}
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>

	<h1>{{ $contato->assunto }}</h1>
	<p>Nome: {{ $contato->nome }}</p>
	<p>E-Mail: {{ $contato->email }}</p>
	<p>Mensagem: {{ $contato->mensagem }}</p>

</body>
</html>