<?php $pagina = "Início"; ?>
<?php $menu = "inicio"; ?>

@extends('comum')

@section('corpo')

<section class="slide">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12 nopadding">
				<ul class="bxslider">
					@foreach($slides as $slide)
						<li><img src="{{ asset('public/storage/' . $slide->urlImagem) }}"/></li>
					@endforeach
				</ul>
			</div>
		</div>
	</div>
</section>

<section class="busca">
	<div class="container well">
		<div class="row">
			<div class="col-md-12">
				<h1 class="titulo2">Busque o imóvel ideal</h1>
			</div>
		</div>
		{!! Form::open(array('route' => 'realizar-busca', 'method' => 'POST'), ['class' => 'form-busca']) !!}
		
			<div class="row">
				<div class="col-md-2">
					<div class="form-group">
						<div class="t-form">Código do Imóvel</div>
						<input type="text" name="referencia" id="" class="form-control2">		
					</div>
				</div>

				<div class="col-md-8">
					<div class="form-group">
						<div class="t-form">Nome do Imóvel</div>
						<input type="text" name="nome" id="" class="form-control2">		
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-2">
					<div class="form-group">
						<div class="t-form">Finalidade</div>
						<select name="finalidade" id="" class="form-control2">
							<option value="">-- Selecione --</option>
							<option value="1">Quero comprar</option>
							<option value="2">Quero alugar</option>
						</select>	
					</div>
				</div>

				<div class="col-md-2">
					<div class="form-group">
						<div class="t-form">Tipo do Imóvel</div>
						<select name="tipo_id" id="" class="form-control2">
							<option value="">-- Selecione --</option>
							@foreach($tipos as $tipo)
								<option value="{{$tipo->id}}">{{ ucFirst($tipo->nome) }}</option>
							@endforeach
						</select>	
					</div>
				</div>

				<div class="col-md-2">
					<div class="form-group">
						<div class="t-form">Cidade</div>
						<select name="cidade_id" id="" class="form-control2">
							<option value="">-- Selecione --</option>
							@foreach($cidades as $cidade)
								<option value="{{$cidade->id}}">{{ ucFirst($cidade->nome) }}</option>
							@endforeach
						</select>	
					</div>
				</div>

				<div class="col-md-2">
					<div class="form-group">
						<div class="t-form">Bairro</div>
						<input type="text" name="bairro" id="" class="form-control2">
					</div>
				</div>

				<div class="col-md-2">
					<div class="form-group">
						<div class="t-form">Área em m²</div>
						<input type="text" name="area" id="" class="form-control2">		
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-2">
					<div class="form-group">
						<div class="t-form">Quartos</div>
						<input type="text" name="quartos" id="" class="form-control2">
					</div>
				</div>

				<div class="col-md-2">
					<div class="form-group">
						<div class="t-form">Suítes</div>
						<input type="text" name="suites" id="" class="form-control2">	
					</div>
				</div>

				<div class="col-md-2">
					<div class="form-group">
						<div class="t-form">Banheiros</div>
						<input type="text" name="banheiros" id="" class="form-control2">		
					</div>
				</div>

				<div class="col-md-2">
					<div class="form-group">
						<div class="t-form">Garagens</div>
						<input type="text" name="garagens" id="" class="form-control2">		
					</div>
				</div>

				<div class="col-md-2">
					<div class="form-group">
						<div class="t-form">Valor Mínimo</div>
						<input type="text" name="valorMinimo" id="" class="form-control2">		
					</div>
				</div>

				<div class="col-md-2">
					<div class="form-group">
						<div class="t-form">Valor Máximo</div>
						<input type="text" name="valorMaximo" id="" class="form-control2">
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12"/>
					<button type="submit" class="btn botao btn-sm">Buscar <i class="fa fa-search"></i></button>
				</div>
			</div>
		{!! Form::close() !!}
	</div>
</section>


<?php
	$imoveis = array_chunk($imoveisEmDestaque->toArray(), 3);
?>

<section class="destaque">
	<div class="container">
	
		<div class="row">
			<div class="col-md-12">
				<h1 class="titulo branco">Imóveis em Destaque</h1>
			</div>
		</div>
	
		@foreach($imoveis as $imoveisLinha)
			
			<div class="row">

				@foreach($imoveisLinha as $imovel)
					<!-- INICIO -->
					
					<div class="col-md-4">
							<div class="imovel-exibicao thumbnail">
								<div class="row">
									<div class="col-sm-12">
										<div class="faixa">
											<img src="{{ asset('public/storage/' . $imovel['urlImagem']) }}" alt="" />
											
											@if ($imovel['vendido'] == 1)
												<span class="vendida">
													Vendido 
												</span>
											@else
												<span class="faixa-padrao">
													@if($imovel['finalidade'] == 1)
														Venda
													@else
														Aluguel
													@endif
												</span>
											@endif

										</div>
			
										<div class="espacamento-imovel">
											<div class="titulo-imovel-ex">
												{!! link_to_route('buscarImovel', $imovel['nome'], $imovel['id']) !!}
											</div>
			
											<div class="desc-imovel-ex">
												{!! substr($imovel['descricao'], 0, 255) . "..." !!}
											</div>
											
											@if($imovel['vendido'] == 0)
												<div class="preco-imovel-ex">
													R$ {{ number_format($imovel['valor'],2) }}
												</div>
											@else
												<div class="imovel-vendido">
													Vendida <i class="fa fa-star"></i>
												</div>
											@endif
											
											<div class="localizacao-imovel-ex">
												{{ $imovel['cidade']['nome'] }}
											</div>
			
											<div class="mais-info">
												{!! link_to_route('buscarImovel', 'Mais Informações', $imovel['id'], ['class' => 'btn botao']) !!}
											</div>
			
										</div>
			
										<div class="info-imovel-ex hidden-sm hidden-xs">
											<div class="row">
												<div class="icones-info">
													<div class="col-sm-3">
														<div class="i-metros" data-toggle="tooltip" data-placement="top" title="Metros"> {{ $imovel['area'] }}m²</div>
													</div>
			
													<div class="col-sm-3">
														<div class="i-quartos" data-toggle="tooltip" data-placement="top" title="Quartos">{{ $imovel['quartos'] }}</div>
													</div>
			
													<div class="col-sm-3">
														<div class="i-banheiros" data-toggle="tooltip" data-placement="top" title="Banheiros">{{ $imovel['banheiros'] }}</div>
													</div>
			
													<div class="col-sm-3">
														<div class="i-garagens" data-toggle="tooltip" data-placement="top" title="Garagens">{{ $imovel['garagens'] }}</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
				@endforeach
			</div>
		@endforeach
			
		</div>
	</div>
</section>

@endsection