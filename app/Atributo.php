<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Atributo extends Model
{
    protected $table = "atributos";

    public function imoveis()
    {
        return $this->hasMany('App\Imovel');
    }
}

