<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ImovelEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $contato;
    public $idImovel;
    public $nomeImovel;

    /* 
    * Envia um e-mail ao corretor do imóvel
    *
    */
    public function __construct($contato, $idImovel, $nomeImovel)
    {
        $this->contato = $contato;
        $this->idImovel = $idImovel;
        $this->nomeImovel = $nomeImovel;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
                ->from($this->contato->email)
                ->subject("Dúvida - Imóvel")
                ->view('emails.emailduvida');
    }
}
