<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Imagem extends Model
{
    protected $table = 'imagens';
    
    public function imoveis()
    {
        return $this->belongsToMany('App\Imovel');
    }
}
