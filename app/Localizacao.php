<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Localizacao extends Model
{
    protected $table = "localizacao";
    protected $guarded = ['id'];
}
