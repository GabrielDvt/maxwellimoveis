<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Imovel extends Model
{
    protected $table = 'imoveis';
    protected $guarded = array('id', 'estado_id', 'atributo_id', 'imagens');
    protected $attributes = [
        'status' => 1,
        'destaque' => 0,
        'vendido' => 0
    ];

    public function cidade()
    {
        return $this->belongsTo('App\Cidade');
    }

    public function imagens()
    {
        return $this->belongsToMany('App\Imagem');
    }

    public function atributos()
    {
        return $this->belongsToMany('App\Atributo');
    }

}
