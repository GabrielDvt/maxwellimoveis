<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Configuracao;
use App\Tipo;
use App\Imovel;
use App\Servico;
use App\Simulacao;
use View;

use Illuminate\Support\Facades\Auth;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //View::share('dados', $dados);
        View::composer('*', function($view) {
            $dados = Configuracao::first();
            $tipos = Tipo::all();
            $servicos = Servico::all();
            $user = Auth::user();
            $simulacoes = Simulacao::all();
            $view->with('dados', $dados)->with('tipos', $tipos)->with('servicos', $servicos)->with('user', $user)->with('simulacoes', $simulacoes);
        });


        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
