<?php

namespace App;

use App\Cidade;
use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
    protected $table = "estados";

    public function cidades()
    {
        return $this->hasMany('App\Cidade');
    }

}
