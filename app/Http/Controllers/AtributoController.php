<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Atributo;

class AtributoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $atributos = Atributo::orderBy('id', 'desc')->get();
        return view('painel.imovel.atributos.atributos')->with('atributos', $atributos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $nome = $request['nome'];
        
        $atributo = new Atributo();
        $atributo->nome = $nome;

        $atributo->save();

        return redirect()->back()->with("success", "Atributo inserido com sucesso!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $atributo = Atributo::find($request['id']);
        $atributo->nome = $request['nome'];
        $atributo->save();
        
        return redirect()->back()->with('success', 'Atributo alterado com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        $atributo = Atributo::destroy($id);
        return redirect()->back()->with('success', 'Atributo excluído com sucesso!');
    }
}
