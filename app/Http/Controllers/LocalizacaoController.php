<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Localizacao;
use Validator;

class LocalizacaoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $localizacoes = Localizacao::all();
        return view('painel.onde-estamos.onde-estamos')->with('localizacoes', $localizacoes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'unidade' => 'required|max:255',
            'telefone' => 'required|max:255',
            'whatsapp' => 'max: 255',
            'email' => 'email|max: 255',
            'endereco' => 'max:255',
            'linkMapa' => 'max:255'
            
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $localizacao = new Localizacao();
        $localizacao->fill($request->all());
        $localizacao->save();

        return redirect()->route('onde-estamos.index')->with('success', 'Localização adicionada com sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'unidade' => 'required|max:255',
            'telefone' => 'required|max:255',
            'whatsapp' => 'max: 255',
            'email' => 'email|max: 255',
            'endereco' => 'max:255',
            'linkMapa' => 'max:255'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $localizacao = Localizacao::find($request['id']);
        $localizacao->fill($request->all());
        $localizacao->save();

        return redirect()->route('onde-estamos.index')->with('success', 'Localização alterada com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tipo = Localizacao::destroy($id);
        return redirect()->route('onde-estamos.index')->with('success', 'Localização excluída com sucesso!');
    }
}
