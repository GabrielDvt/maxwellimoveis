<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Imovel;
use Validator;
use App\Tipo;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Imagem;
use App\Cidade;
use App\Estado;
use Intervention\Image\ImageManagerStatic as Image;
use App\Atributo;

class ImovelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $imoveis = Imovel::orderBy('id', 'desc')->get();
        return view('painel.imovel.ver-imoveis.ver-imoveis')->with('imoveis', $imoveis);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tipos = Tipo::orderBy('id', 'desc')->get();
        $estados = Estado::orderBy('id', 'desc')->get();
        $atributos = Atributo::orderBy('id', 'desc')->get();

        return view('painel.imovel.ver-imoveis.adicionar')->with('tipos', $tipos)->with('estados', $estados)->with('atributos', $atributos);
    }

    public function alterarPosicaoDestaque(Request $request) 
    {
        $id = $request->input('id');
        $posicao = $request->input('posicaoDestaque');


        $imovel = Imovel::find($id);
        $imovel->posicaoDestaque = $posicao;
        $imovel->save();

        return redirect()->back()->with('success', 'Posição alterada com sucesso!');
    }

    public function alterarStatus($id)
    {
        $imovel = Imovel::find($id);
        if ($imovel->status == 0)
            $imovel->status = 1;
        else 
            $imovel->status = 0;
            
        $imovel->save();

        return redirect()->back()->with('success', 'Status alterado com sucesso!');
    }

    public function alterarDestaque($id)
    {
        $imovel = Imovel::find($id);
        if ($imovel->destaque == 0)
            $imovel->destaque = 1;
        else 
            $imovel->destaque = 0;
            
        $imovel->save();

        return redirect()->back()->with('success', 'Destaque alterado com sucesso!');
    }

    public function ordenarDestaques() 
    {
        $imoveis = Imovel::where('destaque', 1)->get();
        return view('painel.imovel.ver-imoveis.ordenarDestaque')->with('imoveis', $imoveis);
    }

    public function alterarVendido($id)
    {
        $imovel = Imovel::find($id);
        if ($imovel->vendido == 0)
            $imovel->vendido = 1;
        else 
            $imovel->vendido = 0;
            
        $imovel->save();

        return redirect()->back()->with('msg', 'Alterado com sucesso!');
    }

    public function alterarCapa(Request $request) 
    {
        $imagem_id = $request->input('imagem_id');
        $imovel_id = $request->input('imovel_id');
        
        $imovel = Imovel::find($imovel_id);
        $imovel->capa = $imagem_id;
        $imovel->save();

        return redirect()->back();
    }

    public function excluirImagem(Request $request)
    {
        $imagem_id = $request->input('imagem_id');
        $imovel_id = $request->input('imovel_id');

        $imovel = Imovel::find($imovel_id);
        //verifica se o ID da imagem é igual ao ID da capa. Caso positivo, excluir a capa
        if ($imovel->capa == $imagem_id) {
            $imovel->capa = null;
            $imovel->save();
        }

        $imovel->imagens()->detach($imagem_id);

        return redirect()->back();
    }

    public function buscarCapa($id) 
    {
        $imovel_id = $id;
        $imovel = Imovel::find($imovel_id);
        $imagem = Imagem::find($imovel->capa);
        $urlImagem = $imagem->urlImagem;

        return $urlImagem;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //modelar com try catch depois
        $imovel = Imovel::whereId($id)
                    ->with('imagens')
                    ->with('cidade')
                    ->with('atributos')
                    ->firstOrFail();

        return view('imovel')->with('imovel', $imovel);
    }


    public function imoveisPorTipo($id)
    {
        $imoveis = Imovel::where('tipo_id', $id)
                    ->where('status', 1)
                    ->with('cidade')
                    ->join('imagens', 'imoveis.capa', 'imagens.id')
                    ->select('imoveis.*', 'imagens.urlImagem')
                    ->get();
        
        $tipo = Tipo::find($id);

        return view('imoveis')
                ->with('imoveis', $imoveis)
                ->with('titulo', $tipo['nome']);
    }


    public function imoveisPorFinalidade($id)
    {
        $imoveis = Imovel::where('finalidade', $id)
                    ->where('status', 1)
                    ->with('cidade')
                    ->join('imagens', 'imoveis.capa', 'imagens.id')
                    ->select('imoveis.*', 'imagens.urlImagem')
                    ->get();

        if ($id == 1) {
            $finalidade = "Quero Comprar";
        } else {
            $finalidade = "Quero Alugar";
        }
        return view('imoveis')->with('imoveis', $imoveis)->with('titulo', $finalidade);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tipos = Tipo::pluck('nome', 'id');
        
        $estados = Estado::orderBy('id', 'desc')->get();
        
        $atributos = Atributo::orderBy('id', 'desc')->get();
        
        $imovel = Imovel::whereId($id)->with('imagens')->with('atributos')->first();

        $imagemCapa = Imagem::find($imovel->capa);

        $cidade_id = $imovel->cidade_id;
        $objEstado = Cidade::find($cidade_id);
        
        //atributos que não possuem no imóvel
        $diff = $atributos->diff($imovel->atributos);
        //atributos que possuem no imóvel
        $intersect = $atributos->intersect($imovel->atributos);
        
        return view('painel.imovel.ver-imoveis.editar')
            ->with('imovel', $imovel)
            ->with('tipos', $tipos)
            ->with('atributos', $atributos)
            ->with('estados', $estados)
            ->with('diff', $diff)
            ->with('intersect', $intersect)
            ->with('imagemCapa', $imagemCapa)
            ->with('cidade_id', $cidade_id)
            ->with('objEstado', $objEstado);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nome' => 'required|max:255',
            'valor' => 'required|numeric|min: 0',
            'finalidade' => 'max: 255',
            'area' => 'required|numeric|min:0',
            'quartos' => 'required|numeric|min:0',
            'banheiros' => 'required|numeric|min:0',
            'garagens' => 'required|numeric|min:0',
            'suites' => 'required|numeric|min:0',
            'imagens' => 'required',
            'imagens.*' => 'image',
            'referencia' => 'required|unique:imoveis'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput()->with('idCidade', $request->input('cidade_id'));
        }

        $imovel = new Imovel();

        /* pega os dados da view */ 
        $imovel->fill($request->all());
        $atributos = $request->input('atributo_id');

        $count = 0;

        /* faz o upload dos arquivos */
        foreach($request->file('imagens') as $imagemAtual) {

            $fileName = str_slug(str_random(32));

            $extension = $imagemAtual->getClientOriginalExtension();

            $watermark = Image::make('storage/app/public/img/watermark.png')->resize(65, 50)->opacity(70);
            
            /* Com marca d'água
            $img = Image::make($imagemAtual)->insert($watermark, 'bottom-right', 10, 10)->save('public/storage/imagens/imoveis/' . $fileName . '.' . $extension);
            */
            $img = Image::make($imagemAtual)->save('public/storage/imagens/imoveis/' . $fileName . '.' . $extension);
            
            //uso do intervention para adicionar a marca d'água
            
            /** salva a url do arquivo na tabela de imagens */
            $imagem = new Imagem();
            $imagem->urlImagem = "imagens/imoveis/" . $fileName . '.' . $extension;

            $imagem->save();
            
            //salva a imagem da capa como a primeira foto
            if ($count == 0) {
                $imovel->capa = $imagem->id;
                $imovel->save();
                $count++;
            }

            /* relaciona a tabela de imagens com a tabela de imóveis */
            $imovel->imagens()->attach($imagem->id);
        }

        //verifica se existem atributos.
        if (is_array($atributos) || is_object($atributos)) {
            /* relaciona os atributos com o novo imóvel */
            foreach($atributos as $atributo) {
                $imovel->atributos()->attach($atributo);
            }
        }

        return redirect()->back()->with('success', 'Imóvel inserido com sucesso!');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'nome' => 'required|max:255',
            'valor' => 'required|numeric|min: 0',
            'finalidade' => 'max: 255',
            'area' => 'required|numeric|min:0',
            'quartos' => 'required|numeric|min:0',
            'banheiros' => 'required|numeric|min:0',
            'garagens' => 'required|numeric|min:0',
            'suites' => 'required|numeric|min:0',
            'imagens.*' => 'image',
            'referencia' => 'required|unique:imoveis'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput()->with('idCidade', $request->input('cidade_id'));
        }

        $imovel = Imovel::find($id);

        /* pega os dados da view */ 
        $imovel->fill($request->all());
        $atributos = $request->input('atributo_id');

        /* salva os dados do imóvel */
        $imovel->save();

        if ($request->hasFile('imagens')) {
            /* faz o upload dos arquivos */
            foreach($request->file('imagens') as $imagemAtual) {
                $fileName = str_slug(str_random(32));

                $extension = $imagemAtual->getClientOriginalExtension();

                $watermark = Image::make('img/logo.png')->fit(70, 40);
                
                /*
                $img = Image::make($imagemAtual)->insert($watermark, 'bottom-right', 10, 10)->save('public/storage/imagens/imoveis/' . $fileName . '.' . $extension);
                */
                $img = Image::make($imagemAtual)->save('public/storage/imagens/imoveis/' . $fileName . '.' . $extension);

                //uso do intervention para adicionar a marca d'água
                
                /** salva a url do arquivo na tabela de imagens */
                $imagem = new Imagem();
                $imagem->urlImagem = "imagens/imoveis/" . $fileName . '.' . $extension;

                $imagem->save();
                
                /* relaciona a tabela de imagens com a tabela de imóveis */
                $imovel->imagens()->attach($imagem->id);
            }
        }
       

        /* relaciona os atributos com o novo imóvel */
        $imovel->atributos()->sync($atributos);
        

        return redirect()->back()->with('success', 'Imóvel alterado com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Imovel::destroy($id);
        return redirect()->back()->with('success', 'Excluído com sucesso!');
    }
}
