<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Configuracao;
use Validator;

class ConfiguracaoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $configuracao = Configuracao::first();
        
        if ($configuracao === null) {
            
            $configuracao = new Configuracao();
            $configuracao->nome = "";
            $configuracao->slogan = "";
            $configuracao->email = "";
            $configuracao->telefone = "";
            $configuracao->whatsapp="";
            $configuracao->descricaoGoogle = "";
            $configuracao->palavrasChave = "";
        }
        return view('painel.configuracoes.configuracoes')->with('configuracao', $configuracao);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nome' => 'required|max:255',
            'slogan' => 'required|max:255',
            'email' => 'required|max: 255|email',
            'telefone' => 'max:255',
            'whatsapp' => 'max:255',
            'descricaoGoogle' => 'max:255',
            'palavras-chave' => 'max:255'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        //caso nenhuma configuração ainda tenha sido criada
        if (Configuracao::count() == 0) {
            $configuracao = new Configuracao();
        } else {    //caso já exista uma configuração, desejo que a configuração atual seja apenas substituída
            $configuracao = Configuracao::first();
        }

        //salva a imagem, caso exista
        if ($request->file('imagem')) {
            $extension = $request->file('imagem')->getClientOriginalExtension();
            Storage::deleteDirectory('img');
            $request->file('imagem')->storeAs('/img', 'watermark' . '.' . $extension);
        }

        $configuracao->fill($request->except('imagem'));
        $configuracao->save();
        return redirect()->back()->with('success', 'Configuração inserida com sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
