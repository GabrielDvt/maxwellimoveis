<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slide;
use Validator;
use Illuminate\Support\Facades\Storage;

class SlideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $slides = Slide::get();
        return view('painel.slides.slides')->with('slides', $slides);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nome' => 'required|max:255',
            'imagem' => 'required',
            'imagem.*' => 'image'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        /* upload da imagem */
        $urlImagem = $request->file('imagem')->store('imagens/slides');

        $slide = new Slide();
        $slide->nome = $request->input('nome');
        $slide->urlImagem = $urlImagem;

        $slide->save();

        return redirect()->back()->with('success', "Slide adicionado com sucesso");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nome' => 'required|max:255',
            'imagem' => 'required',
            'imagem.*' => 'image'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $slide = Slide::find($request['id']);
      
        $urlImagemAntiga = $slide->urlImagem;
        //remove a imagem antiga
        Storage::delete($urlImagemAntiga);
        //armazena a nova imagem
        $urlImagem = $request->file('imagem')->store('imagens/slides');
        
        $slide->nome = $request->input('nome');
        $slide->urlImagem = $urlImagem;

        $slide->save();

        return redirect()->back()->with('success', 'Slide alterado com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Slide::destroy($id);
        return redirect()->back()->with('success', 'Slide excluído com sucesso!');
    }
}
