<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tipo;
use App\Imovel;
use App\Depoimento;
use DB;
use App\Localizacao;
use App\Slide;
use App\Cidade;
use App\Configuracao;

class MainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $tipos = Tipo::orderBy('nome', 'asc')->get();
       
        //busca os imóveis em destaque
       
        $imoveisEmDestaque = Imovel::where('destaque', '=', '1')
                            ->where('status', '=', '1')
                            ->orderBy('posicaoDestaque', 'asc')
                            ->with('cidade')
                            ->join('imagens', 'imoveis.capa', 'imagens.id')
                            ->select('imoveis.*', 'imagens.urlImagem')
                            ->get();
                            
        $slides = Slide::orderBy('id', 'desc')->get();

        //cidades que já possuem um imóvel cadastrado
        $imoveisCadastrados = Imovel::select(DB::raw('count(*) as total, cidade_id'))->groupBy('cidade_id')->get();
        
        $arrayCidades = [];
        
        foreach($imoveisCadastrados as $imovel) {
            $cidade = Cidade::find($imovel->cidade_id);
            array_push($arrayCidades, $cidade);
        }

        return view('index')->with('tipos', compact($tipos))
            ->with('imoveisEmDestaque', $imoveisEmDestaque)
            ->with('slides', $slides)
            ->with('cidades', $arrayCidades);
    }

    public function realizarBusca(Request $request)
    {
        //verifica se o código do imóvel foi passado
        if (!empty($request['referencia'])) {
            $imovel = Imovel::where('referencia', '=', $request['referencia'])->first();
            //caso o código do imóvel tenha sido passado, temos o imóvel desejado
            return redirect()->route('ver-imoveis.show', ['id' => $imovel->id]);    
        }

        //verifica se o nome do imóvel foi informado
        if (!empty($request['nome'])) {
            $nome = $request['nome'];
            $imoveis = Imovel::where('nome', 'like', '%' . $nome . '%')
                        ->join('imagens', 'imoveis.capa', 'imagens.id')
                        ->get();

            return view('imoveis')->with('imoveis', $imoveis)->with('titulo', "Filtro:  Nome: " . $nome);
        }
        
        $filtered_request = array_where($request->except('_token'), function($value, $key) {
            return !is_null($value);
        });
        
        $imoveis = Imovel::where(
            $filtered_request
        )->join('imagens', 'imoveis.capa', 'imagens.id')
         ->get();

        //falta o valor
        return view('imoveis')->with('imoveis', $imoveis)->with('titulo', "Filtro: ");

        //verifica se o nome do imóvel foi passado
        //dd($request->all());
    }

    public function depoimentos()
    {
        $depoimentos = Depoimento::orderBy('id', 'desc')->get();
        return view('depoimentos')->with('depoimentos', $depoimentos);
    }

    public function localizacoes()
    {
        $localizacoes = Localizacao::orderBy('id', 'desc')->get();
        return view('onde-estamos')->with('unidades', $localizacoes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
