<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Simulacao;

class SimulacaoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $simulacoes = Simulacao::get();

        return view('painel.simulacoes.simulacoes')->with('simulacoes', $simulacoes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $simulacao = new Simulacao();
        $simulacao->fill($request->all());
        $simulacao->save();
        return redirect()->back()->with('success', "Simulação inserida com sucesso!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $simulacao = Simulacao::find($id);
        return view('simulacao')->with('simulacao', $simulacao);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $simulacao = Simulacao::find($request->input('id'));
        $simulacao->fill($request->all());
        $simulacao->save();
        return redirect()->back()->with('success', "Simulação alterada com sucesso!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Simulacao::destroy($id);
        return redirect()->back()->with('success', 'Excluído com sucesso!');
    }
}
