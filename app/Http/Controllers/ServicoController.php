<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Servico;
use Validator;
use Illuminate\Support\Facades\Storage;

class ServicoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $servicos = Servico::get();
        return view('painel.servicos.servicos')->with('servicos', $servicos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nome' => 'required|max:255',
            'descricao' => 'required',
            'imagem' => 'required',
            'imagem.*' => 'image'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        /* upload da imagem */
        $urlImagem = $request->file('imagem')->store('imagens/servicos');

        $servico = new Servico();
        $servico->nome = $request->input('nome');
        $servico->descricao = $request->input('descricao');
        $servico->urlImagem = $urlImagem;

        $servico->save();

        return redirect()->back()->with('success', "Serviço adicionado com sucesso");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $servico = Servico::find($id);
        $servicos = Servico::get();
        
        return view('servico')->with('servico', $servico)->with('servicos', $servico);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nome' => 'required|max:255',
            'descricao' => 'required',
            'imagem.*' => 'image'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $servico = Servico::find($request['id']);
        $urlImagemAntiga = $servico->urlImagem;
        
        if ($request->hasFile('imagem')) {
            //remove a imagem antiga
            Storage::delete($urlImagemAntiga);
            //armazena a nova imagem
            $urlImagem = $request->file('imagem')->store('imagens/servicos');
        } else {
            $urlImagem = $urlImagemAntiga;
        }
        
        $servico->nome = $request->input('nome');
        $servico->descricao = $request->input('descricao');
        $servico->urlImagem = $urlImagem;

        $servico->save();

        return redirect()->back()->with('success', 'Serviço alterado com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Servico::destroy($id);
        return redirect()->back()->with('success', 'Serviço excluído com sucesso!');
    }
}
