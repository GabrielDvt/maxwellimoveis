<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Depoimento;
use Validator;

class DepoimentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $depoimentos = Depoimento::all();
        if ($depoimentos === null) {
            $depoimentos = [];
        }
        return view('painel.depoimentos.depoimentos')->with('depoimentos', $depoimentos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'nomeCliente' => 'required|max:255',
            'depoimento' => 'required|max:500'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $depoimento = new Depoimento();
        $depoimento->fill($request->all());

        $depoimento->save();

        return redirect()->back()->with("success", "Depoimento inserido com sucesso!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nomeCliente' => 'required|max:255',
            'depoimento' => 'required|max:500'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $depoimento = Depoimento::find($request['id']);
        $depoimento->fill($request->all());
        $depoimento->save();

        return redirect()->back()->with('success', 'Depoimento alterado com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dep = Depoimento::destroy($id);
        return redirect()->back()->with('success', 'Depoimento excluído com sucesso!');
    }
}
