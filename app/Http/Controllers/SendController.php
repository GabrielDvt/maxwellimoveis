<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contato;
use App\Configuracao;
use App\Mail\ContatoMail;
use App\Mail\SolicitacaoMail;
use App\Mail\ImovelEmail;
use Illuminate\Support\Facades\Mail;

class SendController extends Controller
{

    public function send(Request $request)
    {
        $contato = new Contato();
        $contato->nome = $request->input('nome');
        $contato->email = $request->input('email');
        $contato->assunto = $request->input('assunto');
        $contato->mensagem = $request->input('mensagem');

        //busca o e-mail cadastrado pela imobiliária
        $config = Configuracao::first();

        //envia o e-mail
        Mail::to($config->email)->send(new ContatoMail($contato));

    	return redirect()->back()->with('success', 'E-mail enviado com sucesso!');
    }

    /* envia um e-mail de dúvida para a corretora do imóvel */
    public function sendEmailCorretor(Request $request)
    {
        $contato = new Contato();
        $contato->nome = $request->input('nome');
        $contato->email = $request->input('email');
        $contato->ddd = $request->input('ddd');
        $contato->telefone = $request->input('telefone');
        $contato->duvida = $request->input('duvida');

        $idImovel = $request->input('idImovel');
        $nomeImovel = $request->input('nomeImovel');

        //busca o e-mail cadastrado pela imobiliária
        $config = Configuracao::first();

        //envia o e-mail
        Mail::to($config->email)->send(new ImovelEmail($contato, $idImovel, $nomeImovel));

        return redirect()->back()->with('success', 'Seu e-mail foi enviado com sucesso! Aguarde e entraremos em contato com você!');
    }

    /* envia a solicitação ("nós ligamos para você no canto direito da tela") */
    public function enviarSolicitacao(Request $request)
    {
        $contato = new Contato();
        $contato->nome = $request->input('nome');
        $contato->email = $request->input('email');
        $contato->ddd = $request->input('ddd');
        $contato->telefone = $request->input('telefone');
        $contato->horario = $request->input('horario');

        $config = Configuracao::first();

        Mail::to($config->email)->send(new SolicitacaoMail($contato));

        return redirect()->back()->with('success', 'Solicitação enviada com sucesso!');

    }

}
