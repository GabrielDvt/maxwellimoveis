<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImoveisTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'imoveis';

    /**
     * Run the migrations.
     * @table Imoveis
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('referencia')->unique();
            $table->integer('tipo_id')->unsigned();
            $table->string('nome');
            $table->float('valor', 15, 2);
            $table->integer('cidade_id')->unsigned();
            $table->text('descricao')->nullable();
            $table->integer('finalidade')->nullable();
            $table->float('area');
            $table->integer('quartos');
            $table->integer('banheiros');
            $table->integer('garagens');
            $table->integer('suites');
            $table->integer('status')->nullable()->default(1);
            $table->integer('destaque')->nullable()->default(0);
            $table->integer('vendido')->nullable()->default(0);
            $table->string('bairro')->nullable();
            $table->integer('capa')->unsigned()->nullable();
            $table->integer('posicaoDestaque')->nullable();

            $table->foreign('capa')->references('id')->on('imagens')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('tipo_id')
                ->references('id')->on('tipos')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('cidade_id')
                ->references('id')->on('cidades')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
