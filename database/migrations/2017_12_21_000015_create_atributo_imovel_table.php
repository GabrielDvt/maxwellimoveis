<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAtributoImovelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('atributo_imovel', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('imovel_id')->unsigned();
            $table->integer('atributo_id')->unsigned();
            $table->timestamps();
        
            $table->foreign('imovel_id')
                ->references('id')
                ->on('imoveis')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('atributo_id')
                ->references('id')
                ->on('atributos')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('atributo_imovel');
    }
}
