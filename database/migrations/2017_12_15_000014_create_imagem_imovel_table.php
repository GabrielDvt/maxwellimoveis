<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagemImovelTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'imagem_imovel';

    /**
     * Run the migrations.
     * @table Imoveis_has_Imagens
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('imovel_id')->unsigned();
            $table->integer('imagem_id')->unsigned();

            $table->foreign('imovel_id')
                ->references('id')->on('imoveis')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('imagem_id')
                ->references('id')->on('imagens')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
