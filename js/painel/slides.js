$(document).on("click", "#edit", function() {
    var obj = $(this).data('obj');
    $("[name='id']").val(obj['id']);
    $("[name='nome']").val(obj['nome']);
});

$(document).on("click", "#verImagem", function() {
	var url = $(this).data('url');
    $("#imagem").attr('src', url);
});

function confirmDelete() {
    return confirm("Deseja realmente excluir este atributo?");
}