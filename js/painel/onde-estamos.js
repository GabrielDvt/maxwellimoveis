$(document).on("click", "#editLocalizacao", function () {
    var obj = $(this).data('obj');
    
    $("[name='id']").val(obj['id']);
    $("[name='unidade']").val(obj['unidade']);
    $("[name='telefone']").val(obj['telefone']);
    $("[name='whatsapp']").val(obj['whatsapp']);
    $("[name='email']").val(obj['email']);
    $("[name='endereco']").val(obj['endereco']);
    $("[name='linkMapa']").val(obj['linkMapa']);
});

function confirmDelete() {
    return confirm("Deseja realmente excluir?");    
}