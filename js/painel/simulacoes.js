$(document).on("click", "#edit", function () {
    var obj = $(this).data('obj');

    $("[name='id']").val(obj['id']);
    $("[name='nome']").val(obj['nome']);
    $("[name='link']").val(obj['link']);

});

function confirmDelete() {
    return confirm("Deseja realmente excluir esta simulação?");    
}