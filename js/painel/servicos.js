$(document).on("click", "#edit", function() {
    var obj = $(this).data('obj');
    $("[name='id']").val(obj['id']);
    $("[name='nome']").val(obj['nome']);
    $("#summernoteDescricao").summernote("code", obj['descricao']);
});

function confirmDelete() {
    return confirm("Deseja realmente excluir este atributo?");
}