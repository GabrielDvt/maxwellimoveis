$(document).on("click", "#edit", function () {
    var obj = $(this).data('obj');

    $("[name='id']").val(obj['id']);
    $("[name='nomeCliente']").val(obj['nomeCliente']);
    $("[name='depoimento']").val(obj['depoimento']);
});

function confirmDelete() {
    return confirm("Deseja realmente excluir este atributo?");    
}