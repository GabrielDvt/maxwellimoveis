$(document).on("click", "#editAtributo", function () {
    var id = $(this).data('id');
    var nome = $(this).data('nome');
    $("#id").val(id);
    $("#nome").val(nome);
});

function confirmDelete() {
    return confirm("Deseja realmente excluir este atributo?");    
}