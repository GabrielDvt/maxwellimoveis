<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
| 
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Home
Route::get('/', ['as' => 'main', 'uses' => 'MainController@index']);

//buscar por tipo do imóvel
Route::get('/imoveis/tipo/{id}', ['as' => 'buscarPorTipo', 'uses' => 'ImovelController@imoveisPorTipo']);

//buscar por finalidade
Route::get('/imoveis/finalidade/{id}', ['as' => 'buscarPorFinalidade', 'uses' => 'ImovelController@imoveisPorFinalidade']);

//buscar um imóvel específico
Route::get('/imovel/{id}', ['as' => 'buscarImovel', 'uses' => 'ImovelController@show']);

Route::get('/servicos', function() {
    return view('servicos');
});

Route::get('/servico/{id}', ['as' => 'buscarServico', 'uses' => 'ServicoController@show']);
Route::get('/simulacao/{id}', ['as' => 'buscarSimulacao', 'uses' => 'SimulacaoController@show']);
Route::get('/depoimentos', ['as' => 'depoimentos', 'uses' => 'MainController@depoimentos']);
Route::get('/contato', ['as' => 'contato', 'uses' => 'ContatoController@index']);
Route::get('/onde-estamos', ['as' => 'onde-estamos', 'uses' => 'MainController@localizacoes']);
Route::get('/empresa', ['as' => 'empresa', 'uses' => 'InstitucionalController@sobreNos']);

//realiza a busca (formulário de busca de imóveis)
Route::post('realizar-busca', ['as' => 'realizar-busca', 'uses' => 'MainController@realizarBusca']);

//Envia um e-mail (aba contato) 
Route::post('/sendContato', ['as' => 'sendContato', 'uses' => 'SendController@send']);

//Envia um e-mail de dúvida para o corretor do imóvel
Route::post('/sendEmailCorretor', ['as' => 'sendEmailCorretor', 'uses' => 'SendController@sendEmailCorretor']);

Route::post('/enviarSolicitacao', ['as' => 'enviarSolicitacao', 'uses' => 'SendController@enviarSolicitacao']);

/* Painel Administrador */
Route::group(['prefix' => 'administrador', 'middleware' => 'auth'], function() {

    Route::get('/', ['as' => 'administrador', 'uses' => function() {
        return view('painel.comum');
    }]);

    //Tipos
    Route::resource('tipos', 'TipoController');
    Route::post('alterarTipo', ['as' => 'alterarTipo', 'uses' => 'TipoController@update']);
    
    //Configurações
    Route::resource('configuracoes', 'ConfiguracaoController');
    
    //Atributos
    Route::resource('atributos', 'AtributoController');
    Route::post('alterarAtributo', ['as' => 'alterarAtributo', 'uses' => 'AtributoController@update']);

    //ver imóveis
    Route::resource('ver-imoveis', 'ImovelController');
    Route::put('alterarPosicaoDestaque', ['as' => 'alterarPosicaoDestaque', 'uses' => 'ImovelController@alterarPosicaoDestaque']);

    //rota que altera a imagem de capa de um imóvel
    Route::post('ver-imoveis/alterarCapa/', ['as' => 'alterar-capa', 'uses' => 'ImovelController@alterarCapa']);

    Route::get('ver-imoveis/buscarCapa/{id}', ['as' => 'buscar-capa', 'uses' => 'ImovelController@buscarCapa']);

    //rota que irá excluir uma imagem
    Route::post('ver-imoveis/excluirImagem', ['as' => 'excluir-imagem', 'uses' => 'ImovelController@excluirImagem']);

    Route::get('ordenarDestaques', ['as' => 'ordenarDestaques', 'uses' => 'ImovelController@ordenarDestaques']);

    //rota que irá alterar o status de um imóvel
    Route::get('alterar-status/{id}', ['as' => 'alterar-status', 'uses' => 'ImovelController@alterarStatus']);
    //rota que irá definir o destaque
    Route::get('alterar-destaque/{id}', ['as' => 'alterar-destaque', 'uses' => 'ImovelController@alterarDestaque']);
    //rota que irá alterar se o imóvel está vendido ou não
    Route::get('alterar-vendido/{id}', ['as' => 'alterar-vendido', 'uses' => 'ImovelController@alterarVendido']);

    //Onde estamos
    Route::resource('onde-estamos', 'LocalizacaoController');
    Route::post('alterarLocalizacao', ['as' => 'alterarLocalizacao', 'uses' => 'LocalizacaoController@update']);

    //Depoimentos
    Route::resource('depoimentos', 'DepoimentoController');
    Route::post('alterarDepoimento', ['as' => 'alterarDepoimento', 'uses' => 'DepoimentoController@update']);

    //Serviços
    Route::resource('/servicos', 'ServicoController');
    Route::post('alterarServico', ['as' => 'alterarServico', 'uses' => 'ServicoController@update']);

    //Slides
    Route::resource('/slides', 'SlideController');
    Route::post('alterarSlide', ['as' => 'alterarSlide', 'uses' => 'SlideController@update']);

    //Institucional
    Route::resource('institucional', 'InstitucionalController');

    Route::get('/adicionar-imovel', function() {
        return view('painel.adicionar-imovel.adicionar-imovel');
    });

    Route::resource('/simulacoes', 'SimulacaoController');
    Route::post('alterarSimulacao', ['as' => 'alterarSimulacao', 'uses' => 'SimulacaoController@update']);
    Route::get('/buscar-cidades/{id}', ['as' => 'buscar-cidades', 'uses' => 'CidadeController@buscarCidades']);
});



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
